﻿
local defaults = {
    height = 30,
    width = 75,
    maxColumns = 4,
    unitsPerColumn = 10,
    MinimapPos = 45,
    checked = nil,
    soloCheck = nil,
    showTank = nil,
    showFriendly = nil,
    showOOM = nil,
    showTankTarget = nil,
    OOMlevel = 10,
    hideTM = nil,
    hideTMMM = nil,
    showTaunts = nil,
    showTaunts2 = nil,
    Alpha1 = 75,
    Alpha2 = 35,
    Alpha3 = 35,
    Alpha4 = 15,
    TMHorsemen = 3,
    TMGluth = 3,
    TMToravon = 4,
    TMGormok = 3,
    TMRazorscale = 3,
    TMThorim = 1,
    TMAlgalon = 4,
    TMDeathwhisper = 2,
    TMDeathbringer = 1,
	TMDogs = 6,
    TMFestergut = 9,
    TMPutricide = 2,
    TMSindragosa = 4,
    TMLichKing = 1,
    TMWspWarn = 1,
    TMSayWarn = nil, 
    TMRaidWarn = nil,
    TMRaidWarnWarn = nil,
    TARGET1 = "Click to Select",
    SHIFTTARGET1 = "Click to Select",
    CTRLTARGET1 = "Click to Select",
    ALTTARGET1 = "Click to Select",
    TARGET2 = "Click to Select",
    SHIFTTARGET2 = "Click to Select",
    CTRLTARGET2 = "Click to Select",
    ALTTARGET2 = "Click to Select",
    TARGET3 = "Click to Select",
    SHIFTTARGET3 = "Click to Select",
    CTRLTARGET3 = "Click to Select",
    ALTTARGET3 = "Click to Select",
    TARGET4 = "Click to Select",
    SHIFTTARGET4 = "Click to Select",
    CTRLTARGET4 = "Click to Select",
    ALTTARGET4 = "Click to Select",
    TARGET5 = "Click to Select",
    SHIFTTARGET5 = "Click to Select",
    CTRLTARGET5 = "Click to Select",
    ALTTARGET5 = "Click to Select",
    SPELL1 = "",
    SHIFTSPELL1 = "",
    CTRLSPELL1 = "",
    ALTSPELL1 = "",
    SPELL2 = "",
    SHIFTSPELL2 = "",
    CTRLSPELL2 = "",
    ALTSPELL2 = "",
    SPELL3 = "",
    SHIFTSPELL3 = "",
    CTRLSPELL3 = "",
    ALTSPELL3 = "",
    SPELL4 = "",
    SHIFTSPELL4 = "",
    CTRLSPELL4 = "",
    ALTSPELL4 = "",
    SPELL5 = "",
    SHIFTSPELL5 = "",
    CTRLSPELL5 = "",
    ALTSPELL5 = "",
	TMMacroCheck = nil,
	TMSpellCheck = 1,
	TMSetButton = 1,
	TMMacroShiftCheck = nil,
	TMSpellShiftCheck = 1,
	TMSetShiftButton = 1,
	TMMacroCtrlCheck = nil,
	TMSpellCtrlCheck = 1,
	TMSetCtrlButton = 1,
	TMMacroAltCheck = nil,
	TMSpellAltCheck = 1,
	TMSetAltButton = 1,
	TMMacroCheck2 = nil,
	TMSpellCheck2 = 1,
	TMSetButton2 = 1,
	TMMacroShiftCheck2 = nil,
	TMSpellShiftCheck2 = 1,
	TMSetShiftButton2 = 1,
	TMMacroCtrlCheck2 = nil,
	TMSpellCtrlCheck2 = 1,
	TMSetCtrlButton2 = 1,
	TMMacroAltCheck2 = nil,
	TMSpellAltCheck2 = 1,
	TMSetAltButton2 = 1,
	TMMacroCheck3 = nil,
	TMSpellCheck3 = 1,
	TMSetButton3 = 1,
	TMMacroShiftCheck3 = nil,
	TMSpellShiftCheck3 = 1,
	TMSetShiftButton3 = 1,
	TMMacroCtrlCheck3 = nil,
	TMSpellCtrlCheck3 = 1,
	TMSetCtrlButton3 = 1,
	TMMacroAltCheck3 = nil,
	TMSpellAltCheck3 = 1,
	TMSetAltButton3 = 1,
	TMMacroCheck4 = nil,
	TMSpellCheck4 = 1,
	TMSetButton4 = 1,
	TMMacroShiftCheck4 = nil,
	TMSpellShiftCheck4 = 1,
	TMSetShiftButton4 = 1,
	TMMacroCtrlCheck4 = nil,
	TMSpellCtrlCheck4 = 1,
	TMSetCtrlButton4 = 1,
	TMMacroAltCheck4 = nil,
	TMSpellAltCheck4 = 1,
	TMSetAltButton4 = 1,
	TMMacroCheck5 = nil,
	TMSpellCheck5 = 1,
	TMSetButton5 = 1,
	TMMacroShiftCheck5 = nil,
	TMSpellShiftCheck5 = 1,
	TMSetShiftButton5 = 1,
	TMMacroCtrlCheck5 = nil,
	TMSpellCtrlCheck5 = 1,
	TMSetCtrlButton5 = 1,
	TMMacroAltCheck5 = nil,
	TMSpellAltCheck5 = 1,
	TMSetAltButton5 = 1,
}

local paladindefaults = {
    height = 30,
    width = 75,
    maxColumns = 4,
    unitsPerColumn = 10,
    MinimapPos = 45,
    checked = nil,
    soloCheck = nil,
    showTank = nil,
    showFriendly = nil,
    showOOM = nil,
    showTankTarget = nil,
    OOMlevel = 10,
    hideTM = nil,
    hideTMMM = nil,
    showTaunts = nil,
    showTaunts2 = nil,
    Alpha1 = 75,
    Alpha2 = 35,
    Alpha3 = 35,
    Alpha4 = 15,
    TMHorsemen = 3,
    TMGluth = 3,
    TMToravon = 4,
    TMGormok = 3,
    TMRazorscale = 3,
    TMAlgalon = 4,
    TMDeathwhisper = 2,
    TMDeathbringer = 1,
    TMFestergut = 9,
    TMPutricide = 2,
    TMSindragosa = 4,
    TMLichKing = 1,
	TMDogs = 6,
    TMWspWarn = 1,
    TMSayWarn = nil, 
    TMRaidWarn = nil,
    TMRaidWarnWarn = nil,
    SPELL1 = GetSpellInfo(62124),--hand of reckoning
    SHIFTSPELL1 = "",
    CTRLSPELL1 = GetSpellInfo(1022),--hand of protection
    ALTSPELL1 = "",
    TARGET1 = "[@mouseovertarget]",
    SHIFTTARGET1 = "Click to Select",
    CTRLTARGET1 = "[@mouseover]",
    ALTTARGET1 = "Click to Select",
    SPELL2 = GetSpellInfo(31789),--righteous defense
    SHIFTSPELL2 = "",
    CTRLSPELL2 = "",
    ALTSPELL2 = "",
    TARGET2 = "[@mouseover]",
    SHIFTTARGET2 = "Click to Select",
    CTRLTARGET2 = "Click to Select",
    ALTTARGET2 = "Click to Select",
    SPELL3 = "",
    SHIFTSPELL3 = "",
    CTRLSPELL3 = "",
    ALTSPELL3 = "",
    TARGET3 = "Click to Select",
    SHIFTTARGET3 = "Click to Select",
    CTRLTARGET3 = "Click to Select",
    ALTTARGET3 = "Click to Select",
    SPELL4 = "",
    SHIFTSPELL4 = "",
    CTRLSPELL4 = "",
    ALTSPELL4 = "",
    TARGET4 = "Click to Select",
    SHIFTTARGET4 = "Click to Select",
    CTRLTARGET4 = "Click to Select",
    ALTTARGET4 = "Click to Select",
    SPELL5 = "",
    SHIFTSPELL5 = "",
    CTRLSPELL5 = "",
    ALTSPELL5 = "",
    TARGET5 = "Click to Select",
    SHIFTTARGET5 = "Click to Select",
    CTRLTARGET5 = "Click to Select",
    ALTTARGET5 = "Click to Select",
	TMMacroCheck = nil,
	TMSpellCheck = 1,
	TMSetButton = 0,
	TMMacroShiftCheck = nil,
	TMSpellShiftCheck = 1,
	TMSetShiftButton = 0,
	TMMacroCtrlCheck = nil,
	TMSpellCtrlCheck = 1,
	TMSetCtrlButton = 1,
	TMMacroAltCheck = nil,
	TMSpellAltCheck = 1,
	TMSetAltButton = 1,
	TMMacroCheck2 = nil,
	TMSpellCheck2 = 1,
	TMSetButton2 = 0,
	TMMacroShiftCheck2 = nil,
	TMSpellShiftCheck2 = 1,
	TMSetShiftButton2 = 1,
	TMMacroCtrlCheck2 = nil,
	TMSpellCtrlCheck2 = 1,
	TMSetCtrlButton2 = 1,
	TMMacroAltCheck2 = nil,
	TMSpellAltCheck2 = 1,
	TMSetAltButton2 = 1,
	TMMacroCheck3 = nil,
	TMSpellCheck3 = 1,
	TMSetButton3 = 0,
	TMMacroShiftCheck3 = nil,
	TMSpellShiftCheck3 = 1,
	TMSetShiftButton3 = 1,
	TMMacroCtrlCheck3 = nil,
	TMSpellCtrlCheck3 = 1,
	TMSetCtrlButton3 = 1,
	TMMacroAltCheck3 = nil,
	TMSpellAltCheck3 = 1,
	TMSetAltButton3 = 1,
	TMMacroCheck4 = nil,
	TMSpellCheck4 = 1,
	TMSetButton4 = 1,
	TMMacroShiftCheck4 = nil,
	TMSpellShiftCheck4 = 1,
	TMSetShiftButton4 = 1,
	TMMacroCtrlCheck4 = nil,
	TMSpellCtrlCheck4 = 1,
	TMSetCtrlButton4 = 1,
	TMMacroAltCheck4 = nil,
	TMSpellAltCheck4 = 1,
	TMSetAltButton4 = 1,
	TMMacroCheck5 = nil,
	TMSpellCheck5 = 1,
	TMSetButton5 = 1,
	TMMacroShiftCheck5 = nil,
	TMSpellShiftCheck5 = 1,
	TMSetShiftButton5 = 1,
	TMMacroCtrlCheck5 = nil,
	TMSpellCtrlCheck5 = 1,
	TMSetCtrlButton5 = 1,
	TMMacroAltCheck5 = nil,
	TMSpellAltCheck5 = 1,
	TMSetAltButton5 = 1,
}

local warriordefaults = {
    height = 30,
    width = 75,
    maxColumns = 4,
    unitsPerColumn = 10,
    MinimapPos = 45,
    checked = nil,
    soloCheck = nil,
    showTank = nil,
    showFriendly = nil,
    showOOM = nil,
    showTankTarget = nil,
    OOMlevel = 10,
    hideTM = nil,
    hideTMMM = nil,
    showTaunts = nil,
    showTaunts2 = nil,
    Alpha1 = 75,
    Alpha2 = 35,
    Alpha3 = 35,
    Alpha4 = 15,
    TMHorsemen = 3,
    TMGluth = 3,
    TMToravon = 4,
    TMGormok = 3,
    TMRazorscale = 3,
    TMAlgalon = 4,
	TMDogs = 6,
    TMDeathwhisper = 2,
    TMDeathbringer = 1,
    TMFestergut = 9,
    TMPutricide = 2,
    TMSindragosa = 4,
    TMLichKing = 1,
    TMWspWarn = 1,
    TMSayWarn = nil, 
    TMRaidWarn = nil,
    TMRaidWarnWarn = nil,
    SPELL1 = GetSpellInfo(355), --taunt
    SHIFTSPELL1 = GetSpellInfo(1161), --challenging shout
	CTRLSPELL1 = "",
    ALTSPELL1 = "",
    TARGET1 = "[@mouseovertarget]",
    SHIFTTARGET1 = "[@mouseovertarget]",
    CTRLTARGET1 = "Click to Select",
    ALTTARGET1 = "Click to Select",
    SPELL2 = GetSpellInfo(3411),--intervene
    SHIFTSPELL2 = "",
    CTRLSPELL2 = "",
    ALTSPELL2 = "",
    TARGET2 = "[@mouseover]",
    SHIFTTARGET2 = "Click to Select",
    CTRLTARGET2 = "Click to Select",
    ALTTARGET2 = "Click to Select",
    SPELL3 = "",
    SHIFTSPELL3 = "",
    CTRLSPELL3 = "",
    ALTSPELL3 = "",
    TARGET3 = "Click to Select",
    SHIFTTARGET3 = "Click to Select",
    CTRLTARGET3 = "Click to Select",
    ALTTARGET3 = "Click to Select",
    SPELL4 = "",
    SHIFTSPELL4 = "",
    CTRLSPELL4 = "",
    ALTSPELL4 = "",
    TARGET4 = "Click to Select",
    SHIFTTARGET4 = "Click to Select",
    CTRLTARGET4 = "Click to Select",
    ALTTARGET4 = "Click to Select",
    SPELL5 = "",
    SHIFTSPELL5 = "",
    CTRLSPELL5 = "",
    ALTSPELL5 = "",
    TARGET5 = "Click to Select",
    SHIFTTARGET5 = "Click to Select",
    CTRLTARGET5 = "Click to Select",
    ALTTARGET5 = "Click to Select",
	TMMacroCheck = nil,
	TMSpellCheck = 1,
	TMSetButton = 0,
	TMMacroShiftCheck = nil,
	TMSpellShiftCheck = 1,
	TMSetShiftButton = 0,
	TMMacroCtrlCheck = nil,
	TMSpellCtrlCheck = 1,
	TMSetCtrlButton = 1,
	TMMacroAltCheck = nil,
	TMSpellAltCheck = 1,
	TMSetAltButton = 1,
	TMMacroCheck2 = nil,
	TMSpellCheck2 = 1,
	TMSetButton2 = 0,
	TMMacroShiftCheck2 = nil,
	TMSpellShiftCheck2 = 1,
	TMSetShiftButton2 = 1,
	TMMacroCtrlCheck2 = nil,
	TMSpellCtrlCheck2 = 1,
	TMSetCtrlButton2 = 1,
	TMMacroAltCheck2 = nil,
	TMSpellAltCheck2 = 1,
	TMSetAltButton2 = 1,
	TMMacroCheck3 = nil,
	TMSpellCheck3 = 1,
	TMSetButton3 = 1,
	TMMacroShiftCheck3 = nil,
	TMSpellShiftCheck3 = 1,
	TMSetShiftButton3 = 1,
	TMMacroCtrlCheck3 = nil,
	TMSpellCtrlCheck3 = 1,
	TMSetCtrlButton3 = 1,
	TMMacroAltCheck3 = nil,
	TMSpellAltCheck3 = 1,
	TMSetAltButton3 = 1,
	TMMacroCheck4 = nil,
	TMSpellCheck4 = 1,
	TMSetButton4 = 1,
	TMMacroShiftCheck4 = nil,
	TMSpellShiftCheck4 = 1,
	TMSetShiftButton4 = 1,
	TMMacroCtrlCheck4 = nil,
	TMSpellCtrlCheck4 = 1,
	TMSetCtrlButton4 = 1,
	TMMacroAltCheck4 = nil,
	TMSpellAltCheck4 = 1,
	TMSetAltButton4 = 1,
	TMMacroCheck5 = nil,
	TMSpellCheck5 = 1,
	TMSetButton5 = 1,
	TMMacroShiftCheck5 = nil,
	TMSpellShiftCheck5 = 1,
	TMSetShiftButton5 = 1,
	TMMacroCtrlCheck5 = nil,
	TMSpellCtrlCheck5 = 1,
	TMSetCtrlButton5 = 1,
	TMMacroAltCheck5 = nil,
	TMSpellAltCheck5 = 1,
	TMSetAltButton5 = 1,
}

local deathknightdefaults = {
    height = 30,
    width = 75,
    maxColumns = 4,
    unitsPerColumn = 10,
    MinimapPos = 45,
    checked = nil,
    soloCheck = nil,
    showTank = nil,
    showFriendly = nil,
    showOOM = nil,
    showTankTarget = nil,
    OOMlevel = 10,
    hideTM = nil,
    hideTMMM = nil,
    SPELL1 = GetSpellInfo(56222),--dark command
    SHIFTSPELL1 = "",
    CTRLSPELL1 = "",
    ALTSPELL1 = "",
    showTaunts = nil,
    showTaunts2 = nil,
    Alpha1 = 75,
    Alpha2 = 35,
    Alpha3 = 35,
    Alpha4 = 15,
    TMHorsemen = 3,
    TMGluth = 3,
    TMToravon = 4,
    TMGormok = 3,
    TMRazorscale = 3,
    TMAlgalon = 4,
    TMDeathwhisper = 2,
    TMDeathbringer = 1,
	TMDogs = 6,
    TMFestergut = 9,
    TMPutricide = 2,
    TMSindragosa = 4,
    TMLichKing = 1,
    TMWspWarn = 1,
    TMSayWarn = nil, 
    TMRaidWarn = nil,
    TMRaidWarnWarn = nil,
    TARGET1 = "[@mouseovertarget]",
    SHIFTTARGET1 = "Click to Select",
    CTRLTARGET1 = "Click to Select",
    ALTTARGET1 = "Click to Select",
    SPELL2 = GetSpellInfo(49576),--death grip
    SHIFTSPELL2 = "",
    CTRLSPELL2 = "",
    ALTSPELL2 = "",
    TARGET2 = "[@mouseovertarget]",
    SHIFTTARGET2 = "Click to Select",
    CTRLTARGET2 = "Click to Select",
    ALTTARGET2 = "Click to Select",
    SPELL3 = "",
    SHIFTSPELL3 = "",
    CTRLSPELL3 = "",
    ALTSPELL3 = "",
    TARGET3 = "Click to Select",
    SHIFTTARGET3 = "Click to Select",
    CTRLTARGET3 = "Click to Select",
    ALTTARGET3 = "Click to Select",
    SPELL4 = "",
    SHIFTSPELL4 = "",
    CTRLSPELL4 = "",
    ALTSPELL4 = "",
    TARGET4 = "Click to Select",
    SHIFTTARGET4 = "Click to Select",
    CTRLTARGET4 = "Click to Select",
    ALTTARGET4 = "Click to Select",
    SPELL5 = "",
    SHIFTSPELL5 = "",
    CTRLSPELL5 = "",
    ALTSPELL5 = "",
    TARGET5 = "Click to Select",
    SHIFTTARGET5 = "Click to Select",
    CTRLTARGET5 = "Click to Select",
    ALTTARGET5 = "Click to Select",
	TMMacroCheck = nil,
	TMSpellCheck = 1,
	TMSetButton =0 ,
	TMMacroShiftCheck = nil,
	TMSpellShiftCheck = 1,
	TMSetShiftButton = 1,
	TMMacroCtrlCheck = nil,
	TMSpellCtrlCheck = 1,
	TMSetCtrlButton = 1,
	TMMacroAltCheck = nil,
	TMSpellAltCheck = 1,
	TMSetAltButton = 1,
	TMMacroCheck2 = nil,
	TMSpellCheck2 = 1,
	TMSetButton2 = 0,
	TMMacroShiftCheck2 = nil,
	TMSpellShiftCheck2 = 1,
	TMSetShiftButton2 = 1,
	TMMacroCtrlCheck2 = nil,
	TMSpellCtrlCheck2 = 1,
	TMSetCtrlButton2 = 1,
	TMMacroAltCheck2 = nil,
	TMSpellAltCheck2 = 1,
	TMSetAltButton2 = 1,
	TMMacroCheck3 = nil,
	TMSpellCheck3 = 1,
	TMSetButton3 = 1,
	TMMacroShiftCheck3 = nil,
	TMSpellShiftCheck3 = 1,
	TMSetShiftButton3 = 1,
	TMMacroCtrlCheck3 = nil,
	TMSpellCtrlCheck3 = 1,
	TMSetCtrlButton3 = 1,
	TMMacroAltCheck3 = nil,
	TMSpellAltCheck3 = 1,
	TMSetAltButton3 = 1,
	TMMacroCheck4 = nil,
	TMSpellCheck4 = 1,
	TMSetButton4 = 1,
	TMMacroShiftCheck4 = nil,
	TMSpellShiftCheck4 = 1,
	TMSetShiftButton4 = 1,
	TMMacroCtrlCheck4 = nil,
	TMSpellCtrlCheck4 = 1,
	TMSetCtrlButton4 = 1,
	TMMacroAltCheck4 = nil,
	TMSpellAltCheck4 = 1,
	TMSetAltButton4 = 1,
	TMMacroCheck5 = nil,
	TMSpellCheck5 = 1,
	TMSetButton5 = 1,
	TMMacroShiftCheck5 = nil,
	TMSpellShiftCheck5 = 1,
	TMSetShiftButton5 = 1,
	TMMacroCtrlCheck5 = nil,
	TMSpellCtrlCheck5 = 1,
	TMSetCtrlButton5 = 1,
	TMMacroAltCheck5 = nil,
	TMSpellAltCheck5 = 1,
	TMSetAltButton5 = 1,
}

local druiddefaults = {
    height = 30,
    width = 75,
    maxColumns = 4,
    unitsPerColumn = 10,
    MinimapPos = 45,
    checked = nil,
    soloCheck = nil,
    showTank = nil,
    showFriendly = nil,
    showOOM = nil,
    showTankTarget = nil,
    OOMlevel = 10,
    hideTM = nil,
    hideTMMM = nil,
    showTaunts = nil,
    showTaunts2 = nil,
    Alpha1 = 75,
    Alpha2 = 35,
    Alpha3 = 35,
    Alpha4 = 15,
    TMHorsemen = 3,
    TMGluth = 3,
    TMToravon = 4,
    TMGormok = 3,
    TMRazorscale = 3,
    TMAlgalon = 4,
    TMDeathwhisper = 2,
    TMDeathbringer = 1,
	TMDogs = 6,
    TMFestergut = 9,
    TMPutricide = 2,
    TMSindragosa = 4,
    TMLichKing = 1,
    TMWspWarn = 1,
    TMSayWarn = nil, 
    TMRaidWarn = nil,
    TMRaidWarnWarn = nil,
    SPELL1 = GetSpellInfo(6795),--growl
    SHIFTSPELL1 = "",
    CTRLSPELL1 = "",
    ALTSPELL1 = "",
    TARGET1 = "[@mouseovertarget]",
    SHIFTTARGET1 = "Click to Select",
    CTRLTARGET1 = "Click to Select",
    ALTTARGET1 = "Click to Select",
    SPELL2 = GetSpellInfo(5209),--challenging roar
    SHIFTSPELL2 = "",
    CTRLSPELL2 = "",
    ALTSPELL2 = "",
    TARGET2 = "[@mouseover]",
    SHIFTTARGET2 = "Click to Select",
    CTRLTARGET2 = "Click to Select",
    ALTTARGET2 = "Click to Select",
    SPELL3 = "",
    SHIFTSPELL3 = "",
    CTRLSPELL3 = "",
    ALTSPELL3 = "",
    TARGET3 = "Click to Select",
    SHIFTTARGET3 = "Click to Select",
    CTRLTARGET3 = "Click to Select",
    ALTTARGET3 = "Click to Select",
    SPELL4 = "",
    SHIFTSPELL4 = "",
    CTRLSPELL4 = "",
    ALTSPELL4 = "",
    TARGET4 = "Click to Select",
    SHIFTTARGET4 = "Click to Select",
    CTRLTARGET4 = "Click to Select",
    ALTTARGET4 = "Click to Select",
    SPELL5 = "",
    SHIFTSPELL5 = "",
    CTRLSPELL5 = "",
    ALTSPELL5 = "",
    TARGET5 = "Click to Select",
    SHIFTTARGET5 = "Click to Select",
    CTRLTARGET5 = "Click to Select",
    ALTTARGET5 = "Click to Select",
	TMMacroCheck = nil,
	TMSpellCheck = 1,
	TMSetButton = 0,
	TMMacroShiftCheck = nil,
	TMSpellShiftCheck = 1,
	TMSetShiftButton = 1,
	TMMacroCtrlCheck = nil,
	TMSpellCtrlCheck = 1,
	TMSetCtrlButton = 1,
	TMMacroAltCheck = nil,
	TMSpellAltCheck = 1,
	TMSetAltButton = 1,
	TMMacroCheck2 = nil,
	TMSpellCheck2 = 1,
	TMSetButton2 = 0,
	TMMacroShiftCheck2 = nil,
	TMSpellShiftCheck2 = 1,
	TMSetShiftButton2 = 1,
	TMMacroCtrlCheck2 = nil,
	TMSpellCtrlCheck2 = 1,
	TMSetCtrlButton2 = 1,
	TMMacroAltCheck2 = nil,
	TMSpellAltCheck2 = 1,
	TMSetAltButton2 = 1,
	TMMacroCheck3 = nil,
	TMSpellCheck3 = 1,
	TMSetButton3 = 1,
	TMMacroShiftCheck3 = nil,
	TMSpellShiftCheck3 = 1,
	TMSetShiftButton3 = 1,
	TMMacroCtrlCheck3 = nil,
	TMSpellCtrlCheck3 = 1,
	TMSetCtrlButton3 = 1,
	TMMacroAltCheck3 = nil,
	TMSpellAltCheck3 = 1,
	TMSetAltButton3 = 1,
	TMMacroCheck4 = nil,
	TMSpellCheck4 = 1,
	TMSetButton4 = 1,
	TMMacroShiftCheck4 = nil,
	TMSpellShiftCheck4 = 1,
	TMSetShiftButton4 = 1,
	TMMacroCtrlCheck4 = nil,
	TMSpellCtrlCheck4 = 1,
	TMSetCtrlButton4 = 1,
	TMMacroAltCheck4 = nil,
	TMSpellAltCheck4 = 1,
	TMSetAltButton4 = 1,
	TMMacroCheck5 = nil,
	TMSpellCheck5 = 1,
	TMSetButton5 = 1,
	TMMacroShiftCheck5 = nil,
	TMSpellShiftCheck5 = 1,
	TMSetShiftButton5 = 1,
	TMMacroCtrlCheck5 = nil,
	TMSpellCtrlCheck5 = 1,
	TMSetCtrlButton5 = 1,
	TMMacroAltCheck5 = nil,
	TMSpellAltCheck5 = 1,
	TMSetAltButton5 = 1,
}


function TauntMaster_Button_OnLoad(self)
    TMbuttonName = self:GetName()
   self:RegisterForDrag("LeftButton")
   self:RegisterForClicks("AnyUp")
   self:RegisterEvent("RAID_ROSTER_UPDATE")
   self:RegisterEvent("PARTY_MEMBERS_CHANGED")
   self:RegisterEvent("PLAYER_ENTERING_WORLD")
   self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
   self.healthbar = getglobal(self:GetName().."_HealthBar")
   self.name = getglobal(self:GetName().."_Name")
   self.tankicon = getglobal(self:GetName().."_TM_Tank_Icon")
   self.tanktargeticon = getglobal(self:GetName().."_TM_TankTarget_Icon")
   self.healicon = getglobal(self:GetName().."_TM_Friendly_Icon")
   self.oomicon = getglobal(self:GetName().."_TM_OOM_Icon")
   self.taunticon = getglobal(self:GetName().."_TM_Taunt_Icon")
   self.cshouticon = getglobal(self:GetName().."_TM_cShout_Icon")
   self.horeckoningicon = getglobal(self:GetName().."_TM_HoReckoning_Icon")
   self.rdefenseicon = getglobal(self:GetName().."_TM_rDefense_Icon")
   self.dcommandicon = getglobal(self:GetName().."_TM_dCommand_Icon")
   self.dgripicon = getglobal(self:GetName().."_TM_dGrip_Icon")
   self.growlicon = getglobal(self:GetName().."_TM_Growl_Icon")
   self.croaricon = getglobal(self:GetName().."_TM_cRoar_Icon")
   self:SetAttribute("type", "macro")
   self:SetAttribute("alt-ctrl-macrotext*", "/assist mouseover")
   local class = select(2, UnitClass('player'))
   self:RegisterEvent('ADDON_LOADED')
   self:RegisterEvent('PLAYER_LOGIN')
   self:RegisterEvent("PLAYER_TARGET_CHANGED")
   self:RegisterEvent("UNIT_SPELLCAST_SENT")
   self:RegisterEvent("PLAYER_REGEN_DISABLED")
   self:RegisterEvent("PLAYER_REGEN_ENABLED")
   self:RegisterEvent("UNIT_TARGET")
end

function TauntMaster_Button_OnEvent(self, event, ...)
    if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
       self:SetWidth(TauntMasterDB.width)
       self:SetHeight(TauntMasterDB.height)
        --[[local SPELL1 = TauntMaster_EditBox:GetText()
        local SHIFTSPELL1 = TauntMaster_ShiftEditBox:GetText()
        local CTRLSPELL1 = TauntMaster_CtrlEditBox:GetText()
        local ALTSPELL1 = TauntMaster_AltEditBox:GetText()
        local SPELL2 = TauntMaster_EditBox2:GetText()
        local SHIFTSPELL2 = TauntMaster_ShiftEditBox2:GetText()
        local CTRLSPELL2 = TauntMaster_CtrlEditBox2:GetText()
        local ALTSPELL2 = TauntMaster_AltEditBox2:GetText()
        local SPELL3 = TauntMaster_EditBox3:GetText()
        local SHIFTSPELL3 = TauntMaster_ShiftEditBox3:GetText()
        local CTRLSPELL3 = TauntMaster_CtrlEditBox3:GetText()
        local ALTSPELL3 = TauntMaster_AltEditBox3:GetText()
        local SPELL4 = TauntMaster_EditBox4:GetText()
        local SHIFTSPELL4 = TauntMaster_ShiftEditBox4:GetText()
        local CTRLSPELL4 = TauntMaster_CtrlEditBox4:GetText()
        local ALTSPELL4 = TauntMaster_AltEditBox4:GetText()
        local SPELL5 = TauntMaster_EditBox5:GetText()
        local SHIFTSPELL5 = TauntMaster_ShiftEditBox5:GetText()
        local CTRLSPELL5 = TauntMaster_CtrlEditBox5:GetText()
        local ALTSPELL5 = TauntMaster_AltEditBox5:GetText()
        local TARGET1 = TauntMasterDBChar.TARGET1
        local SHIFTTARGET1 = ShiftDropDown_Button:GetText()
        local CTRLTARGET1 = CtrlDropDown_Button:GetText()
        local ALTTARGET1 = AltDropDown_Button:GetText()
        local TARGET2 = DropDown2_Button:GetText()
        local SHIFTTARGET2 = ShiftDropDown2_Button:GetText()
        local CTRLTARGET2 = CtrlDropDown2_Button:GetText()
        local ALTTARGET2 = AltDropDown2_Button:GetText()
        local TARGET3 = DropDown3_Button:GetText()
        local SHIFTTARGET3 = ShiftDropDown3_Button:GetText()
        local CTRLTARGET3 = CtrlDropDown3_Button:GetText()
        local ALTTARGET3 = AltDropDown3_Button:GetText()
        local TARGET4 = DropDown4_Button:GetText()
        local SHIFTTARGET4 = ShiftDropDown4_Button:GetText()
        local CTRLTARGET4 = CtrlDropDown4_Button:GetText()
        local ALTTARGET4 = AltDropDown4_Button:GetText()
        local TARGET5 = DropDown5_Button:GetText()
        local SHIFTTARGET5 = ShiftDropDown5_Button:GetText()
        local CTRLTARGET5 = CtrlDropDown5_Button:GetText()
        local ALTTARGET5 = AltDropDown5_Button:GetText()]]
        if TauntMasterDBChar.TMSpellCheck == 1 then
			self:SetAttribute("macrotext1", "/cast "..TauntMasterDBChar.TARGET1 ..TauntMasterDBChar.SPELL1)
		elseif TauntMasterDBChar.TMMacroCheck == 1 then
			self:SetAttribute("macrotext1", TauntMasterDBChar.SPELL1)
		end
        if TauntMasterDBChar.TMSpellShiftCheck == 1 then
			self:SetAttribute("shift-macrotext1", "/cast "..TauntMasterDBChar.SHIFTTARGET1 ..TauntMasterDBChar.SHIFTSPELL1)
		elseif TauntMasterDBChar.TMMacroShiftCheck == 1 then
			self:SetAttribute("shift-macrotext1", TauntMasterDBChar.SHIFTSPELL1)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck == 1 then
			self:SetAttribute("ctrl-macrotext1", "/cast "..TauntMasterDBChar.CTRLTARGET1 ..TauntMasterDBChar.CTRLSPELL1)
		elseif TauntMasterDBChar.TMMacroCtrlCheck == 1 then
			self:SetAttribute("ctrl-macrotext1", TauntMasterDBChar.CTRLSPELL1)
		end
        if TauntMasterDBChar.TMSpellAltCheck == 1 then
			self:SetAttribute("alt-macrotext1", "/cast "..TauntMasterDBChar.ALTTARGET1 ..TauntMasterDBChar.ALTSPELL1)
		elseif TauntMasterDBChar.TMMacroAltCheck == 1 then
			self:SetAttribute("alt-macrotext1", TauntMasterDBChar.ALTSPELL1)
		end
        if TauntMasterDBChar.TMSpellCheck2 == 1 then
			self:SetAttribute("macrotext2", "/cast "..TauntMasterDBChar.TARGET2 ..TauntMasterDBChar.SPELL2)
		elseif TauntMasterDBChar.TMMacroCheck2 == 1 then
			self:SetAttribute("macrotext2", TauntMasterDBChar.SPELL2)
		end
        if TauntMasterDBChar.TMSpellShiftCheck2 == 1 then
			self:SetAttribute("shift-macrotext2", "/cast "..TauntMasterDBChar.SHIFTTARGET2 ..TauntMasterDBChar.SHIFTSPELL2)
		elseif TauntMasterDBChar.TMMacroShiftCheck2 == 1 then
			self:SetAttribute("shift-macrotext2", TauntMasterDBChar.SHIFTSPELL2)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck2 == 1 then
			self:SetAttribute("ctrl-macrotext2", "/cast "..TauntMasterDBChar.CTRLTARGET2 ..TauntMasterDBChar.CTRLSPELL2)
		elseif TauntMasterDBChar.TMMacroCtrlCheck2 == 1 then
			self:SetAttribute("ctrl-macrotext2", TauntMasterDBChar.CTRLSPELL2)
		end
        if TauntMasterDBChar.TMSpellAltCheck2 == 1 then
			self:SetAttribute("alt-macrotext2", "/cast "..TauntMasterDBChar.ALTTARGET2 ..TauntMasterDBChar.ALTSPELL2)
		elseif TauntMasterDBChar.TMMacroAltCheck2 == 1 then
			self:SetAttribute("alt-macrotext2", TauntMasterDBChar.ALTSPELL2)
		end
        if TauntMasterDBChar.TMSpellCheck3 == 1 then
			self:SetAttribute("macrotext3", "/cast "..TauntMasterDBChar.TARGET3 ..TauntMasterDBChar.SPELL3)
		elseif TauntMasterDBChar.TMMacroCheck3 == 1 then
			self:SetAttribute("macrotext3", TauntMasterDBChar.SPELL3)
		end
        if TauntMasterDBChar.TMSpellShiftCheck3 == 1 then
			self:SetAttribute("shift-macrotext3", "/cast "..TauntMasterDBChar.SHIFTTARGET3 ..TauntMasterDBChar.SHIFTSPELL3)
		elseif TauntMasterDBChar.TMMacroShiftCheck3 == 1 then
			self:SetAttribute("shift-macrotext3", TauntMasterDBChar.SHIFTSPELL3)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck3 == 1 then
			self:SetAttribute("ctrl-macrotext3", "/cast "..TauntMasterDBChar.CTRLTARGET3 ..TauntMasterDBChar.CTRLSPELL3)
		elseif TauntMasterDBChar.TMMacroCtrlCheck3 == 1 then
			self:SetAttribute("ctrl-macrotext3", TauntMasterDBChar.CTRLSPELL3)
		end
        if TauntMasterDBChar.TMSpellAltCheck3 == 1 then
			self:SetAttribute("alt-macrotext3", "/cast "..TauntMasterDBChar.ALTTARGET3 ..TauntMasterDBChar.ALTSPELL3)
		elseif TauntMasterDBChar.TMMacroAltCheck3 == 1 then
			self:SetAttribute("alt-macrotext3", TauntMasterDBChar.ALTSPELL3)
		end
        if TauntMasterDBChar.TMSpellCheck4 == 1 then
			self:SetAttribute("macrotext4", "/cast "..TauntMasterDBChar.TARGET4 ..TauntMasterDBChar.SPELL4)
		elseif TauntMasterDBChar.TMMacroCheck4 == 1 then
			self:SetAttribute("macrotext4", TauntMasterDBChar.SPELL4)
		end
        if TauntMasterDBChar.TMSpellShiftCheck4 == 1 then
			self:SetAttribute("shift-macrotext4", "/cast "..TauntMasterDBChar.SHIFTTARGET4 ..TauntMasterDBChar.SHIFTSPELL4)
		elseif TauntMasterDBChar.TMMacroShiftCheck4 == 1 then
			self:SetAttribute("shift-macrotext4", TauntMasterDBChar.SHIFTSPELL4)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck4 == 1 then
			self:SetAttribute("ctrl-macrotext4", "/cast "..TauntMasterDBChar.CTRLTARGET4 ..TauntMasterDBChar.CTRLSPELL4)
		elseif TauntMasterDBChar.TMMacroCtrlCheck4 == 1 then
			self:SetAttribute("ctrl-macrotext4", TauntMasterDBChar.CTRLSPELL4)
		end
        if TauntMasterDBChar.TMSpellAltCheck4 == 1 then
			self:SetAttribute("alt-macrotext4", "/cast "..TauntMasterDBChar.ALTTARGET4 ..TauntMasterDBChar.ALTSPELL4)
		elseif TauntMasterDBChar.TMMacroAltCheck4 == 1 then
			self:SetAttribute("alt-macrotext4", TauntMasterDBChar.ALTSPELL4)
		end
        if TauntMasterDBChar.TMSpellCheck5 == 1 then
			self:SetAttribute("macrotext5", "/cast "..TauntMasterDBChar.TARGET5 ..TauntMasterDBChar.SPELL5)
		elseif TauntMasterDBChar.TMMacroCheck5 == 1 then
			self:SetAttribute("macrotext5", TauntMasterDBChar.SPELL5)
		end
        if TauntMasterDBChar.TMSpellShiftCheck5 == 1 then
			self:SetAttribute("shift-macrotext5", "/cast "..TauntMasterDBChar.SHIFTTARGET5 ..TauntMasterDBChar.SHIFTSPELL5)
		elseif TauntMasterDBChar.TMMacroShiftCheck5 == 1 then
			self:SetAttribute("shift-macrotext5", TauntMasterDBChar.SHIFTSPELL5)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck5 == 1 then
			self:SetAttribute("ctrl-macrotext5", "/cast "..TauntMasterDBChar.CTRLTARGET5 ..TauntMasterDBChar.CTRLSPELL5)
		elseif TauntMasterDBChar.TMMacroCtrlCheck5 == 1 then
			self:SetAttribute("ctrl-macrotext5", TauntMasterDBChar.CTRLSPELL5)
		end
        if TauntMasterDBChar.TMSpellAltCheck5 == 1 then
			self:SetAttribute("alt-macrotext5", "/cast "..TauntMasterDBChar.ALTTARGET5 ..TauntMasterDBChar.ALTSPELL5)
		elseif TauntMasterDBChar.TMMacroAltCheck5 == 1 then
			self:SetAttribute("alt-macrotext5", TauntMasterDBChar.ALTSPELL5)
		end
        --[[self:SetAttribute("macrotext1", "/cast "..TARGET1 ..SPELL1)
        self:SetAttribute("shift-macrotext1", "/cast "..SHIFTTARGET1 ..SHIFTSPELL1)
        self:SetAttribute("ctrl-macrotext1", "/cast "..CTRLTARGET1 ..CTRLSPELL1)
        self:SetAttribute("alt-macrotext1", "/cast "..ALTTARGET1 ..ALTSPELL1)
        self:SetAttribute("macrotext2", "/cast "..TARGET2 ..SPELL2)
        self:SetAttribute("shift-macrotext2", "/cast "..SHIFTTARGET2 ..SHIFTSPELL2)
        self:SetAttribute("ctrl-macrotext2", "/cast "..CTRLTARGET2 ..CTRLSPELL2)
        self:SetAttribute("alt-macrotext2", "/cast "..ALTTARGET2 ..ALTSPELL2)
        self:SetAttribute("macrotext3", "/cast "..TARGET3 ..SPELL3)
        self:SetAttribute("shift-macrotext3", "/cast "..SHIFTTARGET3 ..SHIFTSPELL3)
        self:SetAttribute("ctrl-macrotext3", "/cast "..CTRLTARGET3 ..CTRLSPELL3)
        self:SetAttribute("alt-macrotext3", "/cast "..ALTTARGET3 ..ALTSPELL3)
        self:SetAttribute("macrotext4", "/cast "..TARGET4 ..SPELL4)
        self:SetAttribute("shift-macrotext4", "/cast "..SHIFTTARGET4 ..SHIFTSPELL4)
        self:SetAttribute("ctrl-macrotext4", "/cast "..CTRLTARGET4 ..CTRLSPELL4)
        self:SetAttribute("alt-macrotext4", "/cast "..ALTTARGET4 ..ALTSPELL4)
        self:SetAttribute("macrotext5", "/cast "..TARGET5 ..SPELL5)
        self:SetAttribute("shift-macrotext5", "/cast "..SHIFTTARGET5 ..SHIFTSPELL5)
        self:SetAttribute("ctrl-macrotext5", "/cast "..CTRLTARGET5 ..CTRLSPELL5)
        self:SetAttribute("alt-macrotext5", "/cast "..ALTTARGET5 ..ALTSPELL5)]]
        self.healicon:Hide()
        self.tankicon:Hide()
        self.oomicon:Hide()
        self.taunticon:Hide()
        self.cshouticon:Hide()
        self.horeckoningicon:Hide()
        self.rdefenseicon:Hide()
        self.dcommandicon:Hide()
        self.dgripicon:Hide()
        self.growlicon:Hide()
        self.croaricon:Hide()
        self.tanktargeticon:Hide()
        if TauntMasterDBChar.hideTM == nil then
            TauntMaster_Header:Show()
        elseif TauntMasterDBChar.hideTM == 1 then
            TauntMaster_Header:Hide()
        end
        if TauntMasterDBChar.hideTMMM == nil then
            MiniMap_MinimapButton:Show()
        elseif TauntMasterDBChar.hideTMMM == 1 then
            MiniMap_MinimapButton:Hide()
        end
        
    end
    --local unit = ...

    if event == "PLAYER_LOGIN" then
       DEFAULT_CHAT_FRAME:AddMessage("TauntMaster: Enter /tm for options")
       DEFAULT_CHAT_FRAME:AddMessage("TauntMaster has been updated - MACROS can now be used in place of spell names if desired.  Your settings may have changed, so take a look at the spell bindings menu.  Go to the forum at http://tauntmaster.com to post any bugs.")
    end

    
    
    local unit = self:GetAttribute("unit")
    if (event=="PLAYER_REGEN_DISABLED" or event=="PLAYER_REGEN_ENABLED") then

    local unit = self:GetAttribute("unit")
    tankGuid1 = nil
    tankGuid2 = nil
    tankGuid3 = nil
    tankGuid4 = nil
        tankGuid1Name = nil
        tankGuid2Name = nil
        tankGuid3Name = nil
        tankGuid4Name = nil
        if unit then
    	local unitClass = select(2, UnitClass(unit))
		if unitClass == "WARRIOR" then
			if CheckInteractDistance(unit, 1) then
			if CanInspect(unit) then
			NotifyInspect(unit)
			local shieldLink = GetInventoryItemLink(unit, 17)
                    if shieldLink then
                    local sName, sLink, iRarity, iLevel, iMinLevel, sType, sSubType, iStackCount = GetItemInfo(shieldLink)
                        if sSubType == "Shields" then
                            self.tankicon:Show()
                            self.tankicon:SetAllPoints(self.healthbar)
                            self.tanktargeticon:Hide()
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                        else
                            self.tankicon:Hide()
                        end
                    end
				end
		      end
		    end
        end
		
       --[[ local playerClass = select(2, UnitClass("player"))
		if playerClass == "WARRIOR" then
			--if CheckInteractDistance(unit, 1) then
			--if CanInspect(unit) then
			--NotifyInspect(unit)
			local shieldLink = GetInventoryItemLink(unit, 17)
                    if shieldLink then
                    local sName, sLink, iRarity, iLevel, iMinLevel, sType, sSubType, iStackCount = GetItemInfo(shieldLink)
                        if sSubType == "Shields" then
                            self.tankicon:Show()
                            self.tankicon:SetAllPoints(self.healthbar)
                            self.tanktargeticon:Hide()
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                        else
                            self.tankicon:Hide()
                        end
                    end
				end
                end
				end]]

end
    

    


if event=="COMBAT_LOG_EVENT_UNFILTERED" then
    if self == TauntMaster_HeaderUnitButton1 then
	
	
        local spellId = select(9,...)
        local type = select(2,...)
        local destName = select(7,...)
        local debuffAmount = select(13,...)
        local destGUID = select(6,...)
        local srcName = select(4,...)
        
        	 
        if TauntMasterDB.showTaunts2 == nil and TauntMasterDB.showTaunts ~= nil then --show taunts when on cooldown
          local class = select(2, UnitClass('player'))

            if srcName == UnitName('player') then
                if spellId == 355 then
                    if class == "WARRIOR" then
                        self.taunticon:Show()
                    end
                elseif spellId == 6795 then
                    if class == "DRUID" then
                        self.growlicon:Show()
                    end
                elseif spellId == 56222 then
                    if class == "DEATHKNIGHT" then
                        self.dcommandicon:Show()
                    end
                elseif spellId == 62124 then
                    if class == "PALADIN" then
                        self.horeckoningicon:Show()
                    end
                elseif spellId == 31789 then
                    if class == "PALADIN" then
                        self.rdefenseicon:Show()
                    end
                elseif spellId == 49576 then
                    if class == "DEATHKNIGHT" then
                        self.dgripicon:Show()
                    end
                elseif spellId == 5209 then
                    if class == "DRUID" then
                        self.croaricon:Show()
                    end
                elseif spellId == 1161 then
                    if class == "WARRIOR" then
                        self.cshouticon:Show()
                    end
                end
            end
          end

        if type == "SPELL_AURA_APPLIED_DOSE" then
          if (spellId == 28832 and debuffAmount >= (TMHorsemenEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 28834 and debuffAmount >= (TMHorsemenEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 28833 and debuffAmount >= (TMHorsemenEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 28835 and debuffAmount >= (TMHorsemenEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 54378 and debuffAmount >= (TMGluthEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 71127 and debuffAmount >= (TMDogsEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72004 and debuffAmount >= (TMToravonEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72121 and debuffAmount >= (TMToravonEditBox:GetNumber())) then
                --TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72098 and debuffAmount >= (TMToravonEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72120 and debuffAmount >= (TMToravonEditBox:GetNumber())) then
                --TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 67478 and debuffAmount >= (TMGormokEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 66331 and debuffAmount >= (TMGormokEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 67477 and debuffAmount >= (TMGormokEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 67479 and debuffAmount >= (TMGormokEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 64771 and debuffAmount >= (TMRazorscaleEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 64417 and debuffAmount >= (TMAlgalonEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 64412 and debuffAmount >= (TMAlgalonEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 71204 and debuffAmount >= (TMDeathwhisperEditBox:GetNumber())) then
            if UnitHealthMax(srcName) < 5000000 then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
            elseif UnitHealthMax(srcName) > 10000000 and UnitHealthMax < 20000000 then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
            end
          elseif (spellId == 72219 and debuffAmount == (TMFestergutEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72214 and debuffAmount == (TMFestergutEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72551 and debuffAmount == (TMFestergutEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72552 and debuffAmount == (TMFestergutEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72553 and debuffAmount == (TMFestergutEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72672 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72451 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72454 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72463 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72464 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72506 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72507 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72671 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72745 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72746 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72747 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72748 and debuffAmount >= (TMPutricideEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 70127 and debuffAmount >= (TMSindragosaEditBox:GetNumber())) then
		  	 if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
          elseif (spellId == 72530 and debuffAmount >= (TMSindragosaEditBox:GetNumber())) then
		  	 if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
          elseif (spellId == 72529 and debuffAmount >= (TMSindragosaEditBox:GetNumber())) then
		  	 if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
          elseif (spellId == 72528 and debuffAmount >= (TMSindragosaEditBox:GetNumber())) then
		  	 if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
          elseif (spellId == 70128 and debuffAmount >= (TMSindragosaEditBox:GetNumber())) then
		  	 if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
			 if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnDose(destName, debuffAmount, destGUID)
				end
			 end
          elseif (spellId == 72447 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72408 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72409 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72448 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72449 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 72410 and debuffAmount >= (TMDeathbringerEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 69409 and debuffAmount >= (TMLichKingEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 69410 and debuffAmount >= (TMLichKingEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 73799 and debuffAmount >= (TMLichKingEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 73797 and debuffAmount >= (TMLichKingEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 73798 and debuffAmount >= (TMLichKingEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          elseif (spellId == 62130 and debuffAmount >= (TMThorimEditBox:GetNumber())) then
                TMRaidWarnDose(destName, debuffAmount, destGUID)
          end   
        end
        if type=="SPELL_AURA_APPLIED" then
          if (spellId == 72447) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72410) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72408) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72409) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72448) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72449) then
            if TMDeathbringerEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 69409) then
            if TMLichKingEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 69410) then
            if TMLichKingEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 73799) then
            if TMLichKingEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 73797) then
            if TMLichKingEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 73798) then
            if TMLichKingEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 62130) then
            if TMThorimEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 28832) then
            if TMHorsemenEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 28834) then
            if TMHorsemenEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 28833) then
            if TMHorsemenEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 28835) then
            if TMHorsemenEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 54378) then
            if TMGluthEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 71127) then
            if TMDogsEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72004) then
            if TMToravonEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72120) then
            if TMToravonEditBox:GetNumber() == 1 then
                --TMRaidWarnDose(destName, destGUID)
            end
          elseif (spellId == 72121) then
            if TMToravonEditBox:GetNumber() == 1 then
                --TMRaidWarnDose(destName, destGUID)
            end
          elseif (spellId == 72098) then
            if TMToravonEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 67478) then
            if TMGormokEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 66331) then
            if TMGormokEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 67477) then
            if TMGormokEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 67479) then
            if TMGormokEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 64771) then
            if TMRazorscaleEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 64412) then
            if TMAlgalonEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 64417) then
            if TMAlgalonEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 71204) then
            if TMDeathwhisperEditBox:GetNumber() == 1 then
                if UnitHealthMax(srcName) < 5000000 then
                	TMRaidWarnApplied(destName, destGUID)
                elseif UnitHealthMax(srcName) > 10000000 and UnitHealthMax < 20000000 then
                	TMRaidWarnApplied(destName, destGUID)
                end
            end
          elseif (spellId == 72219) then
            if TMFestergutEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72214) then
            if TMFestergutEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72551) then
            if TMFestergutEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72552) then
            if TMFestergutEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72553) then
            if TMFestergutEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72672) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72748) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72747) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72746) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72745) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72671) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72507) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72506) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72464) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72463) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72454) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 72451) then
            if TMPutricideEditBox:GetNumber() == 1 then
                TMRaidWarnApplied(destName, destGUID)
            end
          elseif (spellId == 70127) then
            if TMSindragosaEditBox:GetNumber() == 1 then
			  if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
            end
          elseif (spellId == 72530) then
            if TMSindragosaEditBox:GetNumber() == 1 then
			  if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
            end
          elseif (spellId == 70529) then
            if TMSindragosaEditBox:GetNumber() == 1 then
			  if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
            end
          elseif (spellId == 70528) then
            if TMSindragosaEditBox:GetNumber() == 1 then
			  if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
            end
          elseif (spellId == 70128) then
            if TMSindragosaEditBox:GetNumber() == 1 then
			  if tankGuid1 ~= nil then
				if tankGuid1 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid2 ~= nil then
				if tankGuid2 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid3 ~= nil then
				if tankGuid3 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
			  if tankGuid4 ~= nil then
				if tankGuid4 == destGUID then
                	TMRaidWarnApplied(destName, destGUID)
				end
			  end
            end

			
			
          end
        end
      end
    end
    
    


end

function TauntMaster_Button_OnShow(self)

  local unit = self:GetAttribute("unit")
  if unit then
  
        if TauntMasterDBChar.TMSpellCheck == 1 then
			self:SetAttribute("macrotext1", "/cast "..TauntMasterDBChar.TARGET1 ..TauntMasterDBChar.SPELL1)
		elseif TauntMasterDBChar.TMMacroCheck == 1 then
			self:SetAttribute("macrotext1", TauntMasterDBChar.SPELL1)
		end
        if TauntMasterDBChar.TMSpellShiftCheck == 1 then
			self:SetAttribute("shift-macrotext1", "/cast "..TauntMasterDBChar.SHIFTTARGET1 ..TauntMasterDBChar.SHIFTSPELL1)
		elseif TauntMasterDBChar.TMMacroShiftCheck == 1 then
			self:SetAttribute("shift-macrotext1", TauntMasterDBChar.SHIFTSPELL1)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck == 1 then
			self:SetAttribute("ctrl-macrotext1", "/cast "..TauntMasterDBChar.CTRLTARGET1 ..TauntMasterDBChar.CTRLSPELL1)
		elseif TauntMasterDBChar.TMMacroCtrlCheck == 1 then
			self:SetAttribute("ctrl-macrotext1", TauntMasterDBChar.CTRLSPELL1)
		end
        if TauntMasterDBChar.TMSpellAltCheck == 1 then
			self:SetAttribute("alt-macrotext1", "/cast "..TauntMasterDBChar.ALTTARGET1 ..TauntMasterDBChar.ALTSPELL1)
		elseif TauntMasterDBChar.TMMacroAltCheck == 1 then
			self:SetAttribute("alt-macrotext1", TauntMasterDBChar.ALTSPELL1)
		end
        if TauntMasterDBChar.TMSpellCheck2 == 1 then
			self:SetAttribute("macrotext2", "/cast "..TauntMasterDBChar.TARGET2 ..TauntMasterDBChar.SPELL2)
		elseif TauntMasterDBChar.TMMacroCheck2 == 1 then
			self:SetAttribute("macrotext2", TauntMasterDBChar.SPELL2)
		end
        if TauntMasterDBChar.TMSpellShiftCheck2 == 1 then
			self:SetAttribute("shift-macrotext2", "/cast "..TauntMasterDBChar.SHIFTTARGET2 ..TauntMasterDBChar.SHIFTSPELL2)
		elseif TauntMasterDBChar.TMMacroShiftCheck2 == 1 then
			self:SetAttribute("shift-macrotext2", TauntMasterDBChar.SHIFTSPELL2)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck2 == 1 then
			self:SetAttribute("ctrl-macrotext2", "/cast "..TauntMasterDBChar.CTRLTARGET2 ..TauntMasterDBChar.CTRLSPELL2)
		elseif TauntMasterDBChar.TMMacroCtrlCheck2 == 1 then
			self:SetAttribute("ctrl-macrotext2", TauntMasterDBChar.CTRLSPELL2)
		end
        if TauntMasterDBChar.TMSpellAltCheck2 == 1 then
			self:SetAttribute("alt-macrotext2", "/cast "..TauntMasterDBChar.ALTTARGET2 ..TauntMasterDBChar.ALTSPELL2)
		elseif TauntMasterDBChar.TMMacroAltCheck2 == 1 then
			self:SetAttribute("alt-macrotext2", TauntMasterDBChar.ALTSPELL2)
		end
        if TauntMasterDBChar.TMSpellCheck3 == 1 then
			self:SetAttribute("macrotext3", "/cast "..TauntMasterDBChar.TARGET3 ..TauntMasterDBChar.SPELL3)
		elseif TauntMasterDBChar.TMMacroCheck3 == 1 then
			self:SetAttribute("macrotext3", TauntMasterDBChar.SPELL3)
		end
        if TauntMasterDBChar.TMSpellShiftCheck3 == 1 then
			self:SetAttribute("shift-macrotext3", "/cast "..TauntMasterDBChar.SHIFTTARGET3 ..TauntMasterDBChar.SHIFTSPELL3)
		elseif TauntMasterDBChar.TMMacroShiftCheck3 == 1 then
			self:SetAttribute("shift-macrotext3", TauntMasterDBChar.SHIFTSPELL3)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck3 == 1 then
			self:SetAttribute("ctrl-macrotext3", "/cast "..TauntMasterDBChar.CTRLTARGET3 ..TauntMasterDBChar.CTRLSPELL3)
		elseif TauntMasterDBChar.TMMacroCtrlCheck3 == 1 then
			self:SetAttribute("ctrl-macrotext3", TauntMasterDBChar.CTRLSPELL3)
		end
        if TauntMasterDBChar.TMSpellAltCheck3 == 1 then
			self:SetAttribute("alt-macrotext3", "/cast "..TauntMasterDBChar.ALTTARGET3 ..TauntMasterDBChar.ALTSPELL3)
		elseif TauntMasterDBChar.TMMacroAltCheck3 == 1 then
			self:SetAttribute("alt-macrotext3", TauntMasterDBChar.ALTSPELL3)
		end
        if TauntMasterDBChar.TMSpellCheck4 == 1 then
			self:SetAttribute("macrotext4", "/cast "..TauntMasterDBChar.TARGET4 ..TauntMasterDBChar.SPELL4)
		elseif TauntMasterDBChar.TMMacroCheck4 == 1 then
			self:SetAttribute("macrotext4", TauntMasterDBChar.SPELL4)
		end
        if TauntMasterDBChar.TMSpellShiftCheck4 == 1 then
			self:SetAttribute("shift-macrotext4", "/cast "..TauntMasterDBChar.SHIFTTARGET4 ..TauntMasterDBChar.SHIFTSPELL4)
		elseif TauntMasterDBChar.TMMacroShiftCheck4 == 1 then
			self:SetAttribute("shift-macrotext4", TauntMasterDBChar.SHIFTSPELL4)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck4 == 1 then
			self:SetAttribute("ctrl-macrotext4", "/cast "..TauntMasterDBChar.CTRLTARGET4 ..TauntMasterDBChar.CTRLSPELL4)
		elseif TauntMasterDBChar.TMMacroCtrlCheck4 == 1 then
			self:SetAttribute("ctrl-macrotext4", TauntMasterDBChar.CTRLSPELL4)
		end
        if TauntMasterDBChar.TMSpellAltCheck4 == 1 then
			self:SetAttribute("alt-macrotext4", "/cast "..TauntMasterDBChar.ALTTARGET4 ..TauntMasterDBChar.ALTSPELL4)
		elseif TauntMasterDBChar.TMMacroAltCheck4 == 1 then
			self:SetAttribute("alt-macrotext4", TauntMasterDBChar.ALTSPELL4)
		end
        if TauntMasterDBChar.TMSpellCheck5 == 1 then
			self:SetAttribute("macrotext5", "/cast "..TauntMasterDBChar.TARGET5 ..TauntMasterDBChar.SPELL5)
		elseif TauntMasterDBChar.TMMacroCheck5 == 1 then
			self:SetAttribute("macrotext5", TauntMasterDBChar.SPELL5)
		end
        if TauntMasterDBChar.TMSpellShiftCheck5 == 1 then
			self:SetAttribute("shift-macrotext5", "/cast "..TauntMasterDBChar.SHIFTTARGET5 ..TauntMasterDBChar.SHIFTSPELL5)
		elseif TauntMasterDBChar.TMMacroShiftCheck5 == 1 then
			self:SetAttribute("shift-macrotext5", TauntMasterDBChar.SHIFTSPELL5)
		end
        if TauntMasterDBChar.TMSpellCtrlCheck5 == 1 then
			self:SetAttribute("ctrl-macrotext5", "/cast "..TauntMasterDBChar.CTRLTARGET5 ..TauntMasterDBChar.CTRLSPELL5)
		elseif TauntMasterDBChar.TMMacroCtrlCheck5 == 1 then
			self:SetAttribute("ctrl-macrotext5", TauntMasterDBChar.CTRLSPELL5)
		end
        if TauntMasterDBChar.TMSpellAltCheck5 == 1 then
			self:SetAttribute("alt-macrotext5", "/cast "..TauntMasterDBChar.ALTTARGET5 ..TauntMasterDBChar.ALTSPELL5)
		elseif TauntMasterDBChar.TMMacroAltCheck5 == 1 then
			self:SetAttribute("alt-macrotext5", TauntMasterDBChar.ALTSPELL5)
		end
		
		
    local class, key = select(2, UnitClass(unit)) or "WARRIOR"  
    local color = RAID_CLASS_COLORS[class]   
    self.name:SetText(UnitName(unit))
    self.name:SetTextColor(color.r, color.g, color.b)
    self.tanktargeticon:Hide()
    self.tankicon:Hide()
    self.oomicon:Hide()

    	local unitClass = select(2, UnitClass(unit))
		if unitClass == "WARRIOR" then
			if CheckInteractDistance(unit, 1) then
			if CanInspect(unit) then
			NotifyInspect(unit)
			local shieldLink = GetInventoryItemLink(unit, 17)
                    if shieldLink then
                    local sName, sLink, iRarity, iLevel, iMinLevel, sType, sSubType, iStackCount = GetItemInfo(shieldLink)
                        if sSubType == "Shields" then
                            self.tankicon:Show()
                            self.tankicon:SetAllPoints(self.healthbar)
                            self.tanktargeticon:Hide()
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                        else
                            self.tankicon:Hide()
                        end
                    end
				end
             end
			 end
			 end
end

local total = 0


function TauntMaster_Button_OnUpdate(self, elapsed)
    if TauntMasterDB.showTaunts == nil and TauntMasterDB.showTaunts2 ~= nil then --show taunts when they are available
        local sTaunt, dTaunt, eTaunt = GetSpellCooldown(355);
        local sCShout, dCShout, eCShout = GetSpellCooldown(1161);
        local sHReckoning, dHReckoning, eHReckoning = GetSpellCooldown(62124);
        local sRDefense, dRDefense, eRDefense = GetSpellCooldown(31789);
        local sDCommand, dDCommand, eDCommand = GetSpellCooldown(56222);
        local sDGrip, dDGrip, eDGrip = GetSpellCooldown(49576);
        local sGrowl, dGrowl, eGrowl = GetSpellCooldown(6795);
        local sCRoar, dCRoar, eCRoar = GetSpellCooldown(5209);
        local class = select(2, UnitClass('player'))
        if class == "WARRIOR" then
            if (sTaunt > 0 and dTaunt > 0) then
                self.taunticon:Hide()
            else
                self.taunticon:Show()
            end
            if (sCShout > 0 and dCShout > 0) then
                self.cshouticon:Hide()
            else
                self.cshouticon:Show()
            end
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "PALADIN" then
            if (sHReckoning > 0 and dHReckoning > 0) then
                self.horeckoningicon:Hide()
            else
                self.horeckoningicon:Show()
            end
            if (sRDefense > 0 and dRDefense > 0) then
                self.rdefenseicon:Hide()
            else
                self.rdefenseicon:Show()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "DEATHKNIGHT" then
            if (sDCommand > 0 and dDCommand > 0) then
                self.dcommandicon:Hide()
            else
                self.dcommandicon:Show()
            end
            if (sDGrip > 0 and dDGrip > 0) then
                self.dgripicon:Hide()
            else
                self.dgripicon:Show()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "DRUID" then
            if (sGrowl > 0 and dGrowl > 0) then
                self.growlicon:Hide()
            else
                self.growlicon:Show()
            end
            if (sCRoar > 0 and dCRoar > 0) then
                self.croaricon:Hide()
            else
                self.croaricon:Show()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
        end
        if class == "WARLOCK" or class == "MAGE" or class == "SHAMAN" or class == "PRIEST" or class == "ROGUE" or class == "HUNTER" then
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
    end
    if TauntMasterDB.showTaunts2 == nil and TauntMasterDB.showTaunts ~= nil then --show taunts when on cooldown
        local sTaunt, dTaunt, eTaunt = GetSpellCooldown(355);
        local sCShout, dCShout, eCShout = GetSpellCooldown(1161);
        local sHReckoning, dHReckoning, eHReckoning = GetSpellCooldown(62124);
        local sRDefense, dRDefense, eRDefense = GetSpellCooldown(31789);
        local sDCommand, dDCommand, eDCommand = GetSpellCooldown(56222);
        local sDGrip, dDGrip, eDGrip = GetSpellCooldown(49576);
        local sGrowl, dGrowl, eGrowl = GetSpellCooldown(6795);
        local sCRoar, dCRoar, eCRoar = GetSpellCooldown(5209);
        local class = select(2, UnitClass('player'))
        if class == "WARRIOR" then
            if (sTaunt == 0 and dTaunt == 0) then
                self.taunticon:Hide()
            end
            if (sCShout == 0 and dCShout == 0) then
                self.cshouticon:Hide()
            end
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "PALADIN" then
            if (sHReckoning == 0 and dHReckoning == 0) then
                self.horeckoningicon:Hide()
            end
            if (sRDefense == 0 and dRDefense == 0) then
                self.rdefenseicon:Hide()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "DEATHKNIGHT" then
            if (sDCommand == 0 and dDCommand == 0) then
                self.dcommandicon:Hide()
            end
            if (sDGrip == 0 and dDGrip == 0) then
                self.dgripicon:Hide()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
        if class == "DRUID" then
            if (sGrowl == 0 and dGrowl == 0) then
                self.growlicon:Hide()
            end
            if (sCRoar == 0 and dCRoar == 0) then
                self.croaricon:Hide()
            end
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
        end
        if class == "WARLOCK" or class == "MAGE" or class == "SHAMAN" or class == "PRIEST" or class == "ROGUE" or class == "HUNTER" then
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end
    end
    
    
    if TauntMasterDB.showTaunts2 == nil and TauntMasterDB.showTaunts == nil then
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
    end

    if TauntMasterDB.showTaunts2 ~= nil and TauntMasterDB.showTaunts ~= nil then
        local class = select(2, UnitClass('player'))
        if class == "WARRIOR" then
                self.taunticon:Show()
                self.cshouticon:Show()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        elseif class == "PALADIN" then
                self.horeckoningicon:Show()
                self.rdefenseicon:Show()
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        elseif class == "DEATHKNIGHT" then
                self.dcommandicon:Show()
                self.dgripicon:Show()
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        elseif class == "DRUID" then
                self.growlicon:Show()
                self.croaricon:Show()
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
        elseif class == "WARLOCK" or class == "MAGE" or class == "SHAMAN" or class == "PRIEST" or class == "ROGUE" or class == "HUNTER" then
            self.taunticon:Hide()
            self.cshouticon:Hide()
            self.horeckoningicon:Hide()
            self.rdefenseicon:Hide()
            self.dcommandicon:Hide()
            self.dgripicon:Hide()
            self.growlicon:Hide()
            self.croaricon:Hide()
        end

    end

    local unit = self:GetAttribute("unit")
    if unit then
      local r, g, b = GetThreatStatusColor(UnitThreatSituation(unit))
      self.healthbar:SetStatusBarColor(r, g, b)
    end
    if self:GetAttribute("unit") == unit then
        self.healthbar:SetValue(UnitHealth(unit))
        self.healthbar:SetMinMaxValues(0, UnitHealthMax(unit))
    end
    
    if TauntMasterDB.showTankTarget == nil then
        if tankGuid1Name ~= nil then
            if UnitIsUnit(unit.."target", tankGuid1Name.."-target") then
                if UnitName(unit) == tankGuid1Name then
                    self.tanktargeticon:Hide()
                else
                self.tanktargeticon:Show()
                self.tanktargeticon:SetAllPoints()
                end
            else
                self.tanktargeticon:Hide()
            end
            
        end
        if tankGuid2Name ~= nil then
            if UnitIsUnit(unit.."target", tankGuid2Name.."-target") then
                if UnitName(unit) == tankGuid2Name then
                    self.tanktargeticon:Hide()
                else
                self.tanktargeticon:Show()
                self.tanktargeticon:SetAllPoints()
                end
            else
                self.tanktargeticon:Hide()
            end
        end
        if tankGuid3Name ~= nil then
            if UnitIsUnit(unit.."target", tankGuid3Name.."-target") then
                if UnitName(unit) == tankGuid3Name then
                    self.tanktargeticon:Hide()
                else
                self.tanktargeticon:Show()
                self.tanktargeticon:SetAllPoints()
                end
            else
                self.tanktargeticon:Hide()
            end
        end
        if tankGuid4Name ~= nil then
            if UnitIsUnit(unit.."target", tankGuid4Name.."-target") then
                if UnitName(unit) == tankGuid4Name then
                    self.tanktargeticon:Hide()
                else
                self.tanktargeticon:Show()
                self.tanktargeticon:SetAllPoints()
                end
            else
                self.tanktargeticon:Hide()
            end
        end
        elseif tankGuid1Name == nil and tankGuid2Name == nil and tankGuid3Name == nil and tankGuid4Name == nil then
            self.tanktargeticon:Hide()
        else
            self.tanktargeticon:Hide()
      end    
          
 if TauntMasterDB.showTank == nil then
    local frostPresence = GetSpellInfo(48263)
    local righteousFury = GetSpellInfo(25781)
    local direBear = GetSpellInfo(9634)
    local bearForm = GetSpellInfo(5487)    
            if UnitInParty(unit) or UnitInRaid(unit) or UnitIsPlayer(unit) then
                local unitClass = select(2, UnitClass(unit))
                if unitClass == "WARRIOR" then

                elseif unitClass == "DEATHKNIGHT" then
                    if UnitBuff(unit, frostPresence)then
                        self.tankicon:Show()
                        self.tankicon:SetAllPoints(self.healthbar)
                        self.tanktargeticon:Hide()
                           --if UnitIsPlayer(unit) then
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                       -- end
                    else
                        self.tankicon:Hide()
                    end
               elseif unitClass == "PALADIN" then
                   if UnitBuff(unit, righteousFury) then
                        self.tankicon:Show()
                        self.tankicon:SetAllPoints(self.healthbar)
                        self.tanktargeticon:Hide()
                           --if UnitIsPlayer(unit) then
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                               if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                        --end
                    else
                        self.tankicon:Hide()
                    end
                elseif unitClass == "DRUID" then
                    if UnitBuff(unit, bearForm) then
                        self.tankicon:Show()
                        self.tankicon:SetAllPoints(self.healthbar)
                        self.tanktargeticon:Hide()
                           --if UnitIsPlayer(unit) then
                            if tankGuid1 == nil then
                                tankGuid1 = UnitGUID(unit)
                                tankGuid1Name = UnitName(unit)
                            elseif tankGuid1 ~= nil then
                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                else
                                    if tankGuid2 == nil then
                                        tankGuid2 = UnitGUID(unit)
                                        tankGuid2Name = UnitName(unit)
                                    elseif tankGuid2 ~= nil then
                                        if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                        else
                                            if tankGuid3 == nil then
                                                tankGuid3 = UnitGUID(unit)
                                                tankGuid3Name = UnitName(unit)
                                            elseif tankGuid3 ~= nil then
                                                if tankGuid1 == UnitGUID(unit) or tankGuid2 == UnitGUID(unit) or tankGuid3 == UnitGUID(unit) or tankGuid4 == UnitGUID(unit) then
                                                else
                                                    if tankGuid4 == nil then
                                                        tankGuid4 = UnitGUID(unit)
                                                        tankGuid4Name = UnitName(unit)
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end

                        --end
                    else
                        self.tankicon:Hide()
                    end
                else
                    self.tankicon:Hide()
                end
            else
                self.tankicon:Hide()
            end
        else
            self.tankicon:Hide()
        end
  if TauntMasterDB.showOOM == nil then
    if UnitInParty(unit) or UnitInRaid(unit) or UnitIsPlayer(unit) then
        local unitsClass = select(2, UnitClass(unit))
        if unitsClass == "PALADIN" or unitsClass == "PRIEST" or unitsClass == "SHAMAN" or unitsClass == "DRUID" or unitsClass == "WARLOCK" or unitsClass == "HUNTER" then
        local power = UnitPower(unit)
        local maxpower = UnitPowerMax(unit)
        local powerType = UnitPowerType(unit)
            if not UnitIsDeadOrGhost(unit) then
              if powerType == 0 then
               if TauntMaster_EditBox_OOM:GetText() ~= "" then
                if power <= (maxpower * (TauntMaster_EditBox_OOM:GetText() / 100)) then
                    self.oomicon:Show()
                    self.oomicon:SetAllPoints(self.healthbar)
                else
                    self.oomicon:Hide()
                end
               end
              else
                self.oomicon:Hide()
              end
            else
                self.oomicon:Hide()
            end
        else
            self.oomicon:Hide()
        end
    end
  else
      self.oomicon:Hide()
  end
    if TauntMasterDB.showFriendly == nil then
        if UnitIsFriend(unit, unit.."target") then
            self.healicon:SetAllPoints(self.healthbar)
            self.healicon:Show()
        else
            self.healicon:Hide()
        end
    else
        self.healicon:Hide()
    end
    if InCombatLockdown() then
        local class = select(2, UnitClass('player'))
        if class == "WARLOCK" then
            local lockSpell = GetSpellInfo(5697)
            local inRange = IsSpellInRange(lockSpell,unit.."target")
                if UnitExists(unit.."target") then
                    if inRange == 1 then
                        if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                            self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                        end
                    elseif inRange == 0 then
                        if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                            self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                        end
                    end  
                else
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "WARRIOR" then
                    local warriorSpell = GetSpellInfo(355)

            local inRange = IsSpellInRange(warriorSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "PALADIN" then
            local paladinSpell = GetSpellInfo(62124)

            local inRange = IsSpellInRange(paladinSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "MAGE" then
             local mageSpell = GetSpellInfo(130)

            local inRange = IsSpellInRange(mageSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "PRIEST" then
             local priestSpell = GetSpellInfo(6346)

            local inRange = IsSpellInRange(priestSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "SHAMAN" then
             local shamanSpell = GetSpellInfo(131)

            local inRange = IsSpellInRange(shamanSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "DEATHKNIGHT" then
             local dkSpell = GetSpellInfo(47476)

            local inRange = IsSpellInRange(dkSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "DRUID" then
              local druidSpell = GetSpellInfo(6795)

            local inRange = IsSpellInRange(druidSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "ROGUE" then
              local rogueSpell = GetSpellInfo(26679)

            local inRange = IsSpellInRange(rogueSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        elseif class == "HUNTER" then
              local hunterSpell = GetSpellInfo(5116)

            local inRange = IsSpellInRange(hunterSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha2:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha2:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha1:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha1:GetText() / 100))
                    end
                end
        end
        
    else
        local class = select(2, UnitClass('player'))
        if class == "WARLOCK" then
                    local lockSpell = GetSpellInfo(5697)

            local inRange = IsSpellInRange(lockSpell,unit.."target")
                if UnitExists(unit.."target") then
                    if inRange == 1 then
                        if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                            self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                        end
                    elseif inRange == 0 then
                        if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                            self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                        end
                    end
                else
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "WARRIOR" then
            local warriorSpell = GetSpellInfo(355)

            local inRange = IsSpellInRange(warriorSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "PALADIN" then
             local paladinSpell = GetSpellInfo(62124)

            local inRange = IsSpellInRange(paladinSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "MAGE" then
             local mageSpell = GetSpellInfo(130)

            local inRange = IsSpellInRange(mageSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "PRIEST" then
             local priestSpell = GetSpellInfo(6346)

            local inRange = IsSpellInRange(priestSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "SHAMAN" then
             local shamanSpell = GetSpellInfo(131)

            local inRange = IsSpellInRange(shamanSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "DEATHKNIGHT" then
             local dkSpell = GetSpellInfo(47476)

            local inRange = IsSpellInRange(dkSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "DRUID" then
              local druidSpell = GetSpellInfo(6795)

            local inRange = IsSpellInRange(druidSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "ROGUE" then
              local rogueSpell = GetSpellInfo(26679)

            local inRange = IsSpellInRange(rogueSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        elseif class == "HUNTER" then
              local hunterSpell = GetSpellInfo(5116)

            local inRange = IsSpellInRange(hunterSpell,unit.."target")
                if inRange == 1 then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                elseif inRange == 0 then
                    if TauntMaster_EditBox_Alpha4:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha4:GetText() / 100))
                    end
                elseif inRange == nil then
                    if TauntMaster_EditBox_Alpha3:GetText() ~= "" then
                        self.healthbar:SetAlpha((TauntMaster_EditBox_Alpha3:GetText() / 100))
                    end
                end
        end

        self:SetWidth(Width:GetValue())
        self:SetHeight(Height:GetValue())
        total = total + elapsed
        if total >= 1 then
          --[[  local SPELL1 = TauntMaster_EditBox:GetText()
            local SHIFTSPELL1 = TauntMaster_ShiftEditBox:GetText()
            local CTRLSPELL1 = TauntMaster_CtrlEditBox:GetText()
            local ALTSPELL1 = TauntMaster_AltEditBox:GetText()
            local SPELL2 = TauntMaster_EditBox2:GetText()
            local SHIFTSPELL2 = TauntMaster_ShiftEditBox2:GetText()
            local CTRLSPELL2 = TauntMaster_CtrlEditBox2:GetText()
            local ALTSPELL2 = TauntMaster_AltEditBox2:GetText()
            local SPELL3 = TauntMaster_EditBox3:GetText()
            local SHIFTSPELL3 = TauntMaster_ShiftEditBox3:GetText()
            local CTRLSPELL3 = TauntMaster_CtrlEditBox3:GetText()
            local ALTSPELL3 = TauntMaster_AltEditBox3:GetText()
            local SPELL4 = TauntMaster_EditBox4:GetText()
            local SHIFTSPELL4 = TauntMaster_ShiftEditBox4:GetText()
            local CTRLSPELL4 = TauntMaster_CtrlEditBox4:GetText()
            local ALTSPELL4 = TauntMaster_AltEditBox4:GetText()
            local SPELL5 = TauntMaster_EditBox5:GetText()
            local SHIFTSPELL5 = TauntMaster_ShiftEditBox5:GetText()
            local CTRLSPELL5 = TauntMaster_CtrlEditBox5:GetText()
            local ALTSPELL5 = TauntMaster_AltEditBox5:GetText()
            local TARGET1 = DropDown_Button:GetText()
            local SHIFTTARGET1 = ShiftDropDown_Button:GetText()
            local CTRLTARGET1 = CtrlDropDown_Button:GetText()
            local ALTTARGET1 = AltDropDown_Button:GetText()
            local TARGET2 = DropDown2_Button:GetText()
            local SHIFTTARGET2 = ShiftDropDown2_Button:GetText()
            local CTRLTARGET2 = CtrlDropDown2_Button:GetText()
            local ALTTARGET2 = AltDropDown2_Button:GetText()
            local TARGET3 = DropDown3_Button:GetText()
            local SHIFTTARGET3 = ShiftDropDown3_Button:GetText()
            local CTRLTARGET3 = CtrlDropDown3_Button:GetText()
            local ALTTARGET3 = AltDropDown3_Button:GetText()
            local TARGET4 = DropDown4_Button:GetText()
            local SHIFTTARGET4 = ShiftDropDown4_Button:GetText()
            local CTRLTARGET4 = CtrlDropDown4_Button:GetText()
            local ALTTARGET4 = AltDropDown4_Button:GetText()
            local TARGET5 = DropDown5_Button:GetText()
            local SHIFTTARGET5 = ShiftDropDown5_Button:GetText()
            local CTRLTARGET5 = CtrlDropDown5_Button:GetText()
            local ALTTARGET5 = AltDropDown5_Button:GetText()
            self:SetAttribute("macrotext1", "/cast "..TARGET1 ..SPELL1)
            self:SetAttribute("shift-macrotext1", "/cast "..SHIFTTARGET1 ..SHIFTSPELL1)
            self:SetAttribute("ctrl-macrotext1", "/cast "..CTRLTARGET1 ..CTRLSPELL1)
            self:SetAttribute("alt-macrotext1", "/cast "..ALTTARGET1 ..ALTSPELL1)
            self:SetAttribute("macrotext2", "/cast "..TARGET2 ..SPELL2)
            self:SetAttribute("shift-macrotext2", "/cast "..SHIFTTARGET2 ..SHIFTSPELL2)
            self:SetAttribute("ctrl-macrotext2", "/cast "..CTRLTARGET2 ..CTRLSPELL2)
            self:SetAttribute("alt-macrotext2", "/cast "..ALTTARGET2 ..ALTSPELL2)
            self:SetAttribute("macrotext3", "/cast "..TARGET3 ..SPELL3)
            self:SetAttribute("shift-macrotext3", "/cast "..SHIFTTARGET3 ..SHIFTSPELL3)
            self:SetAttribute("ctrl-macrotext3", "/cast "..CTRLTARGET3 ..CTRLSPELL3)
            self:SetAttribute("alt-macrotext3", "/cast "..ALTTARGET3 ..ALTSPELL3)
            self:SetAttribute("macrotext4", "/cast "..TARGET4 ..SPELL4)
            self:SetAttribute("shift-macrotext4", "/cast "..SHIFTTARGET4 ..SHIFTSPELL4)
            self:SetAttribute("ctrl-macrotext4", "/cast "..CTRLTARGET4 ..CTRLSPELL4)
            self:SetAttribute("alt-macrotext4", "/cast "..ALTTARGET4 ..ALTSPELL4)
            self:SetAttribute("macrotext5", "/cast "..TARGET5 ..SPELL5)
            self:SetAttribute("shift-macrotext5", "/cast "..SHIFTTARGET5 ..SHIFTSPELL5)
            self:SetAttribute("ctrl-macrotext5", "/cast "..CTRLTARGET5 ..CTRLSPELL5)
            self:SetAttribute("alt-macrotext5", "/cast "..ALTTARGET5 ..ALTSPELL5)]]
            if LockTauntMaster:GetChecked() == nil then
                self:RegisterForDrag("LeftButton")
            elseif LockTauntMaster:GetChecked() == 1 then
                self:RegisterForDrag(nil)
            end
            total = 0
        end
 

          
    end
end


function TauntMaster_Button_OnDragStart(self, button)
  TauntMaster_Header:StartMoving()
  TauntMaster_Header.isMoving = true
end

function TauntMaster_Button_OnDragStop(self, button)
  if TauntMaster_Header.isMoving then
    TauntMaster_Header:StopMovingOrSizing()
  end
end

function TauntMaster_Button_val1_OnLoad(self,vText,Min,Max,Step)
    self.text = vText;
    getglobal(self:GetName().."Text"):SetText(vText);
    getglobal(self:GetName().."Low"):SetText(Min);
    getglobal(self:GetName().."High"):SetText(Max);
    self:SetMinMaxValues(Min,Max);
    self:SetValueStep(Step);
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
	    TauntMasterDB = TauntMasterDB or {}
	    for option, value in pairs(defaults) do
	      if not TauntMasterDB[option] then TauntMasterDB[option] = value end
	    end
        self:SetValue(TauntMasterDB.height)
        FontString6:SetText(TauntMasterDB.height)
	  end
    end)
end

function TauntMaster_Button_val2_OnLoad(self,vText,Min,Max,Step)
    self.text = vText;
    getglobal(self:GetName().."Text"):SetText(vText);
    getglobal(self:GetName().."Low"):SetText(Min);
    getglobal(self:GetName().."High"):SetText(Max);
    self:SetMinMaxValues(Min,Max);
    self:SetValueStep(Step);
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetValue(TauntMasterDB.width)
        FontString7:SetText(TauntMasterDB.width)
	  end
    end)
end

function TauntMaster_Button_val1_OnValueChanged(self)
    TauntMasterDB.height = Height:GetValue()
    FontString6:SetText(self:GetValue())
end

function TauntMaster_Button_val2_OnValueChanged(self)
    TauntMasterDB.width = Width:GetValue()
    FontString7:SetText(self:GetValue())
end

function TauntMaster_Options_OnDragStart(self, button)
  TMConfig:StartMoving()
  TMConfig.isMoving = true
end

function TauntMaster_Options_OnDragStop(self, button)
  if TMConfig.isMoving then
    TMConfig:StopMovingOrSizing()
  end
end

function TauntMaster_Button_val4_OnLoad(self,vText,Min,Max,Step)
    self.text = vText;
    getglobal(self:GetName().."Text"):SetText(vText);
    getglobal(self:GetName().."Low"):SetText(Min);
    getglobal(self:GetName().."High"):SetText(Max);
    self:SetMinMaxValues(Min,Max);
    self:SetValueStep(Step);
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetValue(TauntMasterDB.maxColumns)
        FontString8:SetText(TauntMasterDB.maxColumns)
	  end
    end)
end

function TauntMaster_Button_val5_OnLoad(self,vText,Min,Max,Step)
    self.text = vText;
    getglobal(self:GetName().."Text"):SetText(vText);
    getglobal(self:GetName().."Low"):SetText(Min);
    getglobal(self:GetName().."High"):SetText(Max);
    self:SetMinMaxValues(Min,Max);
    self:SetValueStep(Step);
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetValue(TauntMasterDB.unitsPerColumn)
        FontString9:SetText(TauntMasterDB.unitsPerColumn)
	  end
    end)
end

function TauntMaster_Button_Cols_OnUpdate(self)
    TauntMaster_Header:SetAttribute("maxColumns", TauntMasterDB.maxColumns)
    TauntMaster_Header:SetAttribute("unitsPerColumn", TauntMasterDB.unitsPerColumn)
end

function TauntMaster_Button_val3_OnValueChanged(self)
    TauntMasterDB.maxColumns = maxColumns:GetValue()
    FontString8:SetText(self:GetValue())
    TauntMaster_Header:SetAttribute("maxColumns", TauntMasterDB.maxColumns)
end

function TauntMaster_Button_val4_OnValueChanged(self)
    TauntMasterDB.unitsPerColumn = unitsPerColumn:GetValue()
    FontString9:SetText(self:GetValue())
    TauntMaster_Header:SetAttribute("unitsPerColumn", TauntMasterDB.unitsPerColumn)
end

SLASH_TAUNTMASTER_SLASH_COMMAND1 = "/tauntmaster";
SLASH_TAUNTMASTER_SLASH_COMMAND2 = "/tm";
local function handler(msg, editbox)
 if msg == 'display' then
  TMConfig:Show();
 elseif msg == 'spells' then
  TauntMasterMouseButtons:Show();
 elseif msg == 'show' then
  TauntMaster_Header:Show();
  TauntMasterDBChar.hideTM = nil
 elseif msg == 'hide' then
  TauntMaster_Header:Hide();
  TauntMasterDBChar.hideTM = 1
 elseif msg == 'toggle' then
  if TauntMasterDBChar.hideTM == 1 then
    TauntMaster_Header:Show();
    TauntMasterDBChar.hideTM = nil
  elseif TauntMasterDBChar.hideTM == nil then
    TauntMaster_Header:Hide();
    TauntMasterDBChar.hideTM = 1
  end
  elseif msg == 'raid' or msg == 'warn' then
    TMTauntWarnFrame:Show()
 else
  TMOptionsMenu:Show();
 end
end
SlashCmdList["TAUNTMASTER_SLASH_COMMAND"] = handler;

function MiniMapButton_OnLoad()
   MiniMap_MinimapButton:RegisterForClicks("LeftButtonUp","RightButtonUp")
   MiniMap_MinimapButton:RegisterForDrag("LeftButton","RightButton")
   MiniMap_MinimapButton:RegisterEvent('ADDON_LOADED')
   MiniMap_MinimapButton:SetScript('OnEvent', function(self, event, ...)
     if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
	   TauntMasterDB = TauntMasterDB or {}
       MiniMap_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(TauntMasterDB.MinimapPos)),(80*sin(TauntMasterDB.MinimapPos))-52)
	 end
   end)
end

function MiniMap_MinimapButton_Reposition()
	MiniMap_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(TauntMasterDB.MinimapPos)),(80*sin(TauntMasterDB.MinimapPos))-52)
end


function MiniMap_MinimapButton_DraggingFrame_OnUpdate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UIParent:GetScale()+70 
	ypos = ypos/UIParent:GetScale()-ymin-70

	TauntMasterDB.MinimapPos = math.deg(math.atan2(ypos,xpos)) 
	MiniMap_MinimapButton_Reposition()
end

function LockTauntMaster_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.checked)
	  end
    end)
end

function TMEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetText(TauntMasterDBChar.SPELL1)        
	  end
    end)
end

function TMEditBox_OnCursorChanged(self)
    TauntMasterDBChar.SPELL1 = self:GetText()
	TM_Set_Button:Enable()
    SavedString:SetText("")
end

function TMShiftEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTSPELL1)        
	  end
    end)
end

function TMShiftEditBox_OnCursorChanged(self)
    TauntMasterDBChar.SHIFTSPELL1 = self:GetText()
	TM_Set_ShiftButton:Enable()
    SavedShiftString:SetText("")
end

function TMCtrlEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLSPELL1)        
	  end
    end)
end

function TMCtrlEditBox_OnCursorChanged(self)
    TauntMasterDBChar.CTRLSPELL1 = self:GetText()
	TM_Set_CtrlButton:Enable()
    SavedCtrlString:SetText("")
end

function TMAltEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTSPELL1)        
	  end
    end)
end

function TMAltEditBox_OnCursorChanged(self)
    TauntMasterDBChar.ALTSPELL1 = self:GetText()
	TM_Set_AltButton:Enable()
    SavedAltString:SetText("")
end

function DropDown_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end      
        self:SetText(TauntMasterDBChar.TARGET1)        
	  end
    end)
end

function ShiftDropDown_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTTARGET1)    
	  end
    end)
end

function CtrlDropDown_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLTARGET1)        
	  end
    end)
end

function AltDropDown_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTTARGET1)        
	  end
    end)
end

function TauntMaster_LeftButton_OnDragStart(self, button)
  LeftButtonFrame:StartMoving()
  LeftButtonFrame.isMoving = true
end

function TauntMaster_LeftButton_OnDragStop(self, button)
  if LeftButtonFrame.isMoving then
    LeftButtonFrame:StopMovingOrSizing()
  end
end

function TMEditBox2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SPELL2)
	  end
    end)
end

function TMEditBox2_OnCursorChanged(self)
    TauntMasterDBChar.SPELL2 = self:GetText()
	TM_Set_Button2:Enable()
    SavedString2:SetText("")
end

function TMShiftEditBox2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTSPELL2)        
	  end
    end)
end

function TMShiftEditBox2_OnCursorChanged(self)
    TauntMasterDBChar.SHIFTSPELL2 = self:GetText()
	TM_Set_ShiftButton2:Enable()
    SavedShiftString2:SetText("")
end

function TMCtrlEditBox2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLSPELL2)        
	  end
    end)
end

function TMCtrlEditBox2_OnCursorChanged(self)
    TauntMasterDBChar.CTRLSPELL2 = self:GetText()
	TM_Set_CtrlButton2:Enable()
    SavedCtrlString2:SetText("")
end

function TMAltEditBox2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTSPELL2)        
	  end
    end)
end

function TMAltEditBox2_OnCursorChanged(self)
    TauntMasterDBChar.ALTSPELL2 = self:GetText()
	TM_Set_AltButton2:Enable()
    SavedAltString2:SetText("")
end

function DropDown2_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.TARGET2)        
	  end
    end)
end

function ShiftDropDown2_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTTARGET2)    
	  end
    end)
end

function CtrlDropDown2_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLTARGET2)        
	  end
    end)
end

function AltDropDown2_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTTARGET2)        
	  end
    end)
end

function TauntMaster_RightButton_OnDragStart(self, button)
  RightButtonFrame:StartMoving()
  RightButtonFrame.isMoving = true
end

function TauntMaster_RightButton_OnDragStop(self, button)
  if RightButtonFrame.isMoving then
    RightButtonFrame:StopMovingOrSizing()
  end
end

function TMEditBox3_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SPELL3)        
	  end
    end)
end

function TMEditBox3_OnCursorChanged(self)
    TauntMasterDBChar.SPELL3 = self:GetText()
	TM_Set_Button3:Enable()
    SavedString3:SetText("")
end

function TMShiftEditBox3_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTSPELL3)        
	  end
    end)
end

function TMShiftEditBox3_OnCursorChanged(self)
    TauntMasterDBChar.SHIFTSPELL3 = self:GetText()
	TM_Set_ShiftButton3:Enable()
    SavedShiftString3:SetText("")
end

function TMCtrlEditBox3_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLSPELL3)        
	  end
    end)
end

function TMCtrlEditBox3_OnCursorChanged(self)
    TauntMasterDBChar.CTRLSPELL3 = self:GetText()
	TM_Set_CtrlButton3:Enable()
    SavedCtrlString3:SetText("")
end

function TMAltEditBox3_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTSPELL3)        
	  end
    end)
end

function TMAltEditBox3_OnCursorChanged(self)
    TauntMasterDBChar.ALTSPELL3 = self:GetText()
	TM_Set_AltButton3:Enable()
    SavedAltString3:SetText("")
end

function DropDown3_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.TARGET3)        
	  end
    end)
end

function ShiftDropDown3_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTTARGET3)    
	  end
    end)
end

function CtrlDropDown3_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLTARGET3)        
	  end
    end)
end

function AltDropDown3_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTTARGET3)        
	  end
    end)
end

function TauntMaster_Button3_OnDragStart(self, button)
  ButtonFrame3:StartMoving()
  ButtonFrame3.isMoving = true
end

function TauntMaster_Button3_OnDragStop(self, button)
  if ButtonFrame3.isMoving then
    ButtonFrame3:StopMovingOrSizing()
  end
end

function TMEditBox4_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SPELL4)        
	  end
    end)
end

function TMEditBox4_OnCursorChanged(self)
    TauntMasterDBChar.SPELL4 = self:GetText()
	TM_Set_Button4:Enable()
    SavedString4:SetText("")
end

function TMShiftEditBox4_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTSPELL4)        
	  end
    end)
end

function TMShiftEditBox4_OnCursorChanged(self)
    TauntMasterDBChar.SHIFTSPELL4 = self:GetText()
	TM_Set_ShiftButton4:Enable()
    SavedShiftString4:SetText("")
end

function TMCtrlEditBox4_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLSPELL4)        
	  end
    end)
end

function TMCtrlEditBox4_OnCursorChanged(self)
    TauntMasterDBChar.CTRLSPELL4 = self:GetText()
	TM_Set_CtrlButton4:Enable()
    SavedCtrlString4:SetText("")
end

function TMAltEditBox4_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTSPELL4)        
	  end
    end)
end

function TMAltEditBox4_OnCursorChanged(self)
    TauntMasterDBChar.ALTSPELL4 = self:GetText()
	TM_Set_AltButton4:Enable()
    SavedAltString4:SetText("")
end

function DropDown4_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.TARGET4)        
	  end
    end)
end

function ShiftDropDown4_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTTARGET4)    
	  end
    end)
end

function CtrlDropDown4_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLTARGET4)        
	  end
    end)
end

function AltDropDown4_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTTARGET4)        
	  end
    end)
end

function TauntMaster_Button4_OnDragStart(self, button)
  ButtonFrame4:StartMoving()
  ButtonFrame4.isMoving = true
end

function TauntMaster_Button4_OnDragStop(self, button)
  if ButtonFrame4.isMoving then
    ButtonFrame4:StopMovingOrSizing()
  end
end

function TMEditBox5_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SPELL5)        
	  end
    end)
end

function TMEditBox5_OnCursorChanged(self)
    TauntMasterDBChar.SPELL5 = self:GetText()
	TM_Set_Button5:Enable()
    SavedString5:SetText("")
end

function TMShiftEditBox5_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTSPELL5)        
	  end
    end)
end

function TMShiftEditBox5_OnCursorChanged(self)
    TauntMasterDBChar.SHIFTSPELL5 = self:GetText()
	TM_Set_ShiftButton5:Enable()
    SavedShiftString5:SetText("")
end

function TMCtrlEditBox5_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLSPELL5)        
	  end
    end)
end

function TMCtrlEditBox5_OnCursorChanged(self)
    TauntMasterDBChar.CTRLSPELL5 = self:GetText()
	TM_Set_CtrlButton5:Enable()
    SavedCtrlString5:SetText("")
end

function TMAltEditBox5_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTSPELL5)        
	  end
    end)
end

function TMAltEditBox5_OnCursorChanged(self)
    TauntMasterDBChar.ALTSPELL5 = self:GetText()
	TM_Set_AltButton5:Enable()
    SavedAltString5:SetText("")
end

function DropDown5_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.TARGET5)        
	  end
    end)
end

function ShiftDropDown5_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.SHIFTTARGET5)    
	  end
    end)
end

function CtrlDropDown5_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.CTRLTARGET5)        
	  end
    end)
end

function AltDropDown5_Button_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
      self:SetText(TauntMasterDBChar.ALTTARGET5)        
	  end
    end)
end

function TauntMaster_Button5_OnDragStart(self, button)
  ButtonFrame5:StartMoving()
  ButtonFrame5.isMoving = true
end

function TauntMaster_Button5_OnDragStop(self, button)
  if ButtonFrame5.isMoving then
    ButtonFrame5:StopMovingOrSizing()
  end
end

function TauntMaster_Button_ShowSolo_OnLoad(self)
   self:RegisterEvent('ADDON_LOADED')
   self:SetScript('OnEvent', function(self, event, ...)
     if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
       TauntMasterDB = TauntMasterDB or {}
       for option, value in pairs(defaults) do
         if not TauntMasterDB[option] then TauntMasterDB[option] = value end
       end
       self:SetChecked(TauntMasterDB.soloCheck)
        if TauntMasterDB.soloCheck == nil then
            TauntMaster_Header:SetAttribute("showSolo", true)
        else
            TauntMaster_Header:SetAttribute("showSolo", false)
        end
     end
   end)
end

function TauntMaster_Button_ShowSolo_OnClick(self)
   TauntMasterDB.soloCheck = self:GetChecked()
end

function TauntMaster_Button_ShowSolo_PostClick(self)
    if TauntMasterDB.soloCheck == nil then
        TauntMaster_Header:SetAttribute("showSolo", true)
    else
        TauntMaster_Header:SetAttribute("showSolo", false)
    end
end

function TauntMaster_ShowTankBorder_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.showTank)
	  end
    end)
end

function TauntMaster_ShowFriendlyBorder_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.showFriendly)
	  end
    end)
end

function TauntMaster_ShowOOMBorder_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.showOOM)
	  end
    end)
end

function TauntMaster_ShowTauntIcons_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.showTaunts)
	  end
    end)
end

function TauntMaster_ShowTauntIcons2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetChecked(TauntMasterDB.showTaunts2)
	  end
    end)
end

function TMEditBox_OOM_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
       end
      self:SetText(TauntMasterDB.OOMlevel)        
	  end
    end)
end

function TMEditBox_OOM_OnTextChanged(self)
    TauntMasterDB.OOMlevel = self:GetText()
end

function TMEditBox_Alpha1_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
       end
      self:SetText(TauntMasterDB.Alpha1)        
	  end
    end)
end

function TMEditBox_Alpha1_OnTextChanged(self)
    TauntMasterDB.Alpha1 = self:GetText()
end

function TMEditBox_Alpha2_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
       end
      self:SetText(TauntMasterDB.Alpha2)        
	  end
    end)
end

function TMEditBox_Alpha2_OnTextChanged(self)
    TauntMasterDB.Alpha2 = self:GetText()
end

function TMEditBox_Alpha3_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
       end
      self:SetText(TauntMasterDB.Alpha3)        
	  end
    end)
end

function TMEditBox_Alpha3_OnTextChanged(self)
    TauntMasterDB.Alpha3 = self:GetText()
end

function TMEditBox_Alpha4_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
        else
                TauntMasterDB = TauntMasterDB or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDB[option] then TauntMasterDB[option] = value end
                end
       end
      self:SetText(TauntMasterDB.Alpha4)        
	  end
    end)
end

function TMEditBox_Alpha4_OnTextChanged(self)
    TauntMasterDB.Alpha4 = self:GetText()
end


 	

function TMTauntWarnFrame_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	  end
    end)
end
	
	
	
function TMWspCheckButton_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        if TauntMasterDB.TMWspWarn == 2 then
             self:SetChecked(nil)
         elseif TauntMasterDB.TMWspWarn == 1 then
            self:SetChecked(1)
         end

	  end
    end)

end

function TMWspCheckButton_OnClick(self)
    if self:GetChecked() == nil then
        TauntMasterDB.TMWspWarn = 2
    elseif self:GetChecked() == 1 then
        TauntMasterDB.TMWspWarn = 1
    end
end

function TMSayCheckButton_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
              self:SetChecked(TauntMasterDB.TMSayWarn)

	  end
      end)
end

function TMSayCheckButton_OnClick(self)
    TauntMasterDB.TMSayWarn = self:GetChecked()
end

function TMRaidCheckButton_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
              self:SetChecked(TauntMasterDB.TMRaidWarn)

	  end
    end)
end

function TMRaidCheckButton_OnClick(self)
    TauntMasterDB.TMRaidWarn = self:GetChecked()
end

function TMRaidWarnCheckButton_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
              self:SetChecked(TauntMasterDB.TMRaidWarnWarn)

	  end
    end)
end

function TMRaidWarnCheckButton_OnClick(self)
    TauntMasterDB.TMRaidWarnWarn = self:GetChecked()
end

function TMHorsemenEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
            self:SetText(TauntMasterDB.TMHorsemen)

	  end
    end)
end

function TMHorsemenEditBox_OnTextChanged(self)
    TauntMasterDB.TMHorsemen = self:GetText()
end

function TMGluthEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMGluth)

	  end
    end)
end

function TMGluthEditBox_OnTextChanged(self)
    TauntMasterDB.TMGluth = self:GetText()
end

function TMToravonEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMToravon)

	  end
    end)
end

function TMToravonEditBox_OnTextChanged(self)
    TauntMasterDB.TMToravon = self:GetText()
end

function TMRazorscaleEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMRazorscale)

	  end
    end)
end

function TMRazorscaleEditBox_OnTextChanged(self)
    TauntMasterDB.TMRazorscale = self:GetText()
end

function TMAlgalonEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMAlgalon)

	  end
    end)
end

function TMAlgalonEditBox_OnTextChanged(self)
    TauntMasterDB.TMAlgalon = self:GetText()
end

function TMGormokEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMGormok)

	  end
    end)
end

function TMGormokEditBox_OnTextChanged(self)
    TauntMasterDB.TMGormok = self:GetText()
end

function TMDeathwhisperEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMDeathwhisper)

	  end
    end)
end

function TMDeathwhisperEditBox_OnTextChanged(self)
    TauntMasterDB.TMDeathwhisper = self:GetText()
end

function TMDeathbringerEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMDeathbringer)

	  end
    end)
end

function TMDeathbringerEditBox_OnTextChanged(self)
    TauntMasterDB.TMDeathbringer = self:GetText()
end

function TMFestergutEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
        self:SetText(TauntMasterDB.TMFestergut)

	  end
    end)
end

function TMFestergutEditBox_OnTextChanged(self)
    TauntMasterDB.TMFestergut = self:GetText()
end

function TMPutricideEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	 self:SetText(TauntMasterDB.TMPutricide)

      end
    end)
end

function TMPutricideEditBox_OnTextChanged(self)
    TauntMasterDB.TMPutricide = self:GetText()
end

function TMSindragosaEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	 self:SetText(TauntMasterDB.TMSindragosa)

      end
    end)
end

function TMSindragosaEditBox_OnTextChanged(self)
    TauntMasterDB.TMSindragosa = self:GetText()
end

function TMLichKingEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	  self:SetText(TauntMasterDB.TMLichKing)
      end

    end)
end

function TMLichKingEditBox_OnTextChanged(self)
    TauntMasterDB.TMLichKing = self:GetText()
end

function TMThorimEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	  self:SetText(TauntMasterDB.TMThorim)
      end

    end)
end

function TMThorimEditBox_OnTextChanged(self)
    TauntMasterDB.TMThorim = self:GetText()
end

function TauntMaster_ShowTankTargetBorder_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	  self:SetChecked(TauntMasterDB.showTankTarget)
      end
    end)
end

function TMDogsEditBox_OnLoad(self)
    self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        TauntMasterDB = TauntMasterDB or {}
        for option, value in pairs(defaults) do
          if not TauntMasterDB[option] then TauntMasterDB[option] = value end
        end
	  self:SetText(TauntMasterDB.TMDogs)
      end

    end)
end

function TMDogsEditBox_OnTextChanged(self)
	 TauntMasterDB.TMDogs = self:GetText()
end

function TMRaidWarnDose(destName, debuffAmount, destGUID)
if TauntMasterDB.TMRaidWarn == 1 then
                   SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","RAID");
            end
           if TauntMasterDB.TMRaidWarnWarn == 1 then
                  SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","RAID_WARNING");
           end
           if TauntMasterDB.TMSayWarn == 1 then
                  SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","SAY");
           end
           if TauntMasterDB.TMWspWarn == 1 then
                 if tankGuid1 == destGUID then
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid2 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid3 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid4 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has "..debuffAmount.." stacks!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                 end
            end
end

function TMRaidWarnApplied(destName, destGUID)


			if TauntMasterDB.TMRaidWarn == 1 then
                   SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","RAID");
            end
           if TauntMasterDB.TMRaidWarnWarn == 1 then
                  SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","RAID_WARNING");
           end
           if TauntMasterDB.TMSayWarn == 1 then
                  SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","SAY");
           end
           if TauntMasterDB.TMWspWarn == 1 then
		   

				 
				 
                 if tankGuid1 == destGUID then

				 
				 
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid2 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid3 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                      if tankGuid4 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid4Name);
                      end
                 
                 elseif tankGuid4 == destGUID then
                      if tankGuid1 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid1Name);
                      end
                      if tankGuid3 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid3Name);
                      end
                      if tankGuid2 ~= nil then
                           SendChatMessage(destName.." has 1 stack!  TAUNT NOW!","WHISPER", nil, tankGuid2Name);
                      end
                 end
            end
end



function TM_Cycle(check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton1, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton2, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton3, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton4, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton5, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton6, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton7, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton8, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton9, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton10, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton11, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton12, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton13, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton14, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton15, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton16, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton17, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton18, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton19, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton20, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton21, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton22, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton23, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton24, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton25, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton26, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton27, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton28, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton29, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton30, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton31, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton32, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton33, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton34, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton35, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton36, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton37, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton38, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton39, check1, check2, action, text, target)
    TM_Cycle2(TauntMaster_HeaderUnitButton40, check1, check2, action, text, target)
end

function TM_Cycle2(button, check1, check2, action, text, target)
    if button then
		if check1:GetChecked() == 1 then
			button:SetAttribute(action, "/cast "..target ..text)
		elseif check2:GetChecked() == 1 then
			button:SetAttribute(action, text)
		end
	end
end

function TM_Macro_Check_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCheck)         
	  end
    end)
             
end

function TM_Macro_Check_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_Check:SetChecked(nil)
		DropDown_Button:Hide()
        TauntMasterDBChar.TMMacroCheck = 1
        TauntMasterDBChar.TMSpellCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_Check:SetChecked(1)
		DropDown_Button:Show()
        TauntMasterDBChar.TMMacroCheck = 0
        TauntMasterDBChar.TMSpellCheck = 1
	end
	TM_Set_Button:Enable()
    SavedString:SetText("")
	
end

function TM_Spell_Check_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCheck)        
	  end
    end)
             

end

function TM_Spell_Check_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_Check:SetChecked(nil)
		DropDown_Button:Show()
        TauntMasterDBChar.TMSpellCheck = 1
        TauntMasterDBChar.TMMacroCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_Check:SetChecked(1)
		DropDown_Button:Hide()
        TauntMasterDBChar.TMSpellCheck = 0
        TauntMasterDBChar.TMMacroCheck = 1
	end	
    TM_Set_Button:Enable()
    SavedString:SetText("")
end

function TM_Set_Button_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_Button_OnClick(self)
   self:Disable()
   SavedString:SetText("Saved")
   TM_Cycle(TM_Spell_Check, TM_Macro_Check, "macrotext1", TauntMasterDBChar.SPELL1, TauntMasterDBChar.TARGET1) 
end




function TM_Macro_ShiftCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck:SetChecked(nil)
		ShiftDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck = 1
        TauntMasterDBChar.TMSpellShiftCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck:SetChecked(1)
		ShiftDropDown_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck = 0
        TauntMasterDBChar.TMSpellShiftCheck = 1
	end
	TM_Set_ShiftButton:Enable()
    SavedShiftString:SetText("")
	
end

function TM_Spell_ShiftCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck:SetChecked(nil)
		ShiftDropDown_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck = 1
        TauntMasterDBChar.TMMacroShiftCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck:SetChecked(1)
		ShiftDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck = 0
        TauntMasterDBChar.TMMacroShiftCheck = 1
	end	
    TM_Set_ShiftButton:Enable()
    SavedShiftString:SetText("")
end

function TM_Set_ShiftButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton_OnClick(self)
   self:Disable()
   SavedShiftString:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck, TM_Macro_ShiftCheck, "shift-macrotext1", TauntMasterDBChar.SHIFTSPELL1, TauntMasterDBChar.SHIFTTARGET1) 
end


function TM_Macro_CtrlCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck:SetChecked(nil)
		CtrlDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck = 1
        TauntMasterDBChar.TMSpellCtrlCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck:SetChecked(1)
		CtrlDropDown_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck = 0
        TauntMasterDBChar.TMSpellCtrlCheck = 1
	end
	TM_Set_CtrlButton:Enable()
    SavedCtrlString:SetText("")
	
end

function TM_Spell_CtrlCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck:SetChecked(nil)
		CtrlDropDown_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck = 1
        TauntMasterDBChar.TMMacroCtrlCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck:SetChecked(1)
		CtrlDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck = 0
        TauntMasterDBChar.TMMacroCtrlCheck = 1
	end	
    TM_Set_CtrlButton:Enable()
    SavedCtrlString:SetText("")
end

function TM_Set_CtrlButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton_OnClick(self)
   self:Disable()
   SavedCtrlString:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck, TM_Macro_CtrlCheck, "ctrl-macrotext1", TauntMasterDBChar.CTRLSPELL1, TauntMasterDBChar.CTRLTARGET1) 
end


function TM_Macro_AltCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck)         
	  end
    end)
             
end

function TM_Macro_AltCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck:SetChecked(nil)
		AltDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck = 1
        TauntMasterDBChar.TMSpellAltCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck:SetChecked(1)
		AltDropDown_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck = 0
        TauntMasterDBChar.TMSpellAltCheck = 1
	end
	TM_Set_AltButton:Enable()
    SavedAltString:SetText("")
	
end

function TM_Spell_AltCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck)        
	  end
    end)
             

end

function TM_Spell_AltCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck:SetChecked(nil)
		AltDropDown_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck = 1
        TauntMasterDBChar.TMMacroAltCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck:SetChecked(1)
		AltDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck = 0
        TauntMasterDBChar.TMMacroAltCheck = 1
	end	
    TM_Set_AltButton:Enable()
    SavedAltString:SetText("")
end

function TM_Set_AltButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton_OnClick(self)
   self:Disable()
   SavedAltString:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck, TM_Macro_AltCheck, "alt-macrotext1", TauntMasterDBChar.ALTSPELL1, TauntMasterDBChar.ALTTARGET1) 
end



function TM_Macro_ShiftCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck:SetChecked(nil)
		ShiftDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck = 1
        TauntMasterDBChar.TMSpellShiftCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck:SetChecked(1)
		ShiftDropDown_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck = 0
        TauntMasterDBChar.TMSpellShiftCheck = 1
	end
	TM_Set_ShiftButton:Enable()
    SavedShiftString:SetText("")
	
end

function TM_Spell_ShiftCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck:SetChecked(nil)
		ShiftDropDown_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck = 1
        TauntMasterDBChar.TMMacroShiftCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck:SetChecked(1)
		ShiftDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck = 0
        TauntMasterDBChar.TMMacroShiftCheck = 1
	end	
    TM_Set_ShiftButton:Enable()
    SavedShiftString:SetText("")
end

function TM_Set_ShiftButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton_OnClick(self)
   self:Disable()
   SavedShiftString:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck, TM_Macro_ShiftCheck, "shift-macrotext1", TauntMasterDBChar.SHIFTSPELL1, TauntMasterDBChar.SHIFTTARGET1) 
end


function TM_Macro_CtrlCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck:SetChecked(nil)
		CtrlDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck = 1
        TauntMasterDBChar.TMSpellCtrlCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck:SetChecked(1)
		CtrlDropDown_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck = 0
        TauntMasterDBChar.TMSpellCtrlCheck = 1
	end
	TM_Set_CtrlButton:Enable()
    SavedCtrlString:SetText("")
	
end

function TM_Spell_CtrlCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck:SetChecked(nil)
		CtrlDropDown_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck = 1
        TauntMasterDBChar.TMMacroCtrlCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck:SetChecked(1)
		CtrlDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck = 0
        TauntMasterDBChar.TMMacroCtrlCheck = 1
	end	
    TM_Set_CtrlButton:Enable()
    SavedCtrlString:SetText("")
end

function TM_Set_CtrlButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton_OnClick(self)
   self:Disable()
   SavedCtrlString:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck, TM_Macro_CtrlCheck, "ctrl-macrotext1", TauntMasterDBChar.CTRLSPELL1, TauntMasterDBChar.CTRLTARGET1) 
end


function TM_Macro_AltCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck)         
	  end
    end)
             
end

function TM_Macro_AltCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck:SetChecked(nil)
		AltDropDown_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck = 1
        TauntMasterDBChar.TMSpellAltCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck:SetChecked(1)
		AltDropDown_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck = 0
        TauntMasterDBChar.TMSpellAltCheck = 1
	end
	TM_Set_AltButton:Enable()
    SavedAltString:SetText("")
	
end

function TM_Spell_AltCheck_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck)        
	  end
    end)
             

end

function TM_Spell_AltCheck_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck:SetChecked(nil)
		AltDropDown_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck = 1
        TauntMasterDBChar.TMMacroAltCheck = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck:SetChecked(1)
		AltDropDown_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck = 0
        TauntMasterDBChar.TMMacroAltCheck = 1
	end	
    TM_Set_AltButton:Enable()
    SavedAltString:SetText("")
end

function TM_Set_AltButton_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton_OnClick(self)
   self:Disable()
   SavedAltString:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck, TM_Macro_AltCheck, "alt-macrotext1", TauntMasterDBChar.ALTSPELL1, TauntMasterDBChar.ALTTARGET1) 
end


function TM_Macro_Check2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCheck2)         
	  end
    end)
             
end

function TM_Macro_Check2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_Check2:SetChecked(nil)
		DropDown2_Button:Hide()
        TauntMasterDBChar.TMMacroCheck2 = 1
        TauntMasterDBChar.TMSpellCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_Check2:SetChecked(1)
		DropDown2_Button:Show()
        TauntMasterDBChar.TMMacroCheck2 = 0
        TauntMasterDBChar.TMSpellCheck2 = 1
	end
	TM_Set_Button2:Enable()
    SavedString2:SetText("")
	
end

function TM_Spell_Check2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCheck2)        
	  end
    end)
             

end

function TM_Spell_Check2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_Check2:SetChecked(nil)
		DropDown2_Button:Show()
        TauntMasterDBChar.TMSpellCheck2 = 1
        TauntMasterDBChar.TMMacroCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_Check2:SetChecked(1)
		DropDown2_Button:Hide()
        TauntMasterDBChar.TMSpellCheck2 = 0
        TauntMasterDBChar.TMMacroCheck2 = 1
	end	
    TM_Set_Button2:Enable()
    SavedString2:SetText("")
end

function TM_Set_Button2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_Button2_OnClick(self)
   self:Disable()
   SavedString2:SetText("Saved")
   TM_Cycle(TM_Spell_Check2, TM_Macro_Check2, "macrotext2", TauntMasterDBChar.SPELL2, TauntMasterDBChar.TARGET2) 
end


function TM_Macro_ShiftCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck2)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck2:SetChecked(nil)
		ShiftDropDown2_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck2 = 1
        TauntMasterDBChar.TMSpellShiftCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck2:SetChecked(1)
		ShiftDropDown2_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck2 = 0
        TauntMasterDBChar.TMSpellShiftCheck2 = 1
	end
	TM_Set_ShiftButton2:Enable()
    SavedShiftString2:SetText("")
	
end

function TM_Spell_ShiftCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck2)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck2:SetChecked(nil)
		ShiftDropDown2_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck2 = 1
        TauntMasterDBChar.TMMacroShiftCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck2:SetChecked(1)
		ShiftDropDown2_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck2 = 0
        TauntMasterDBChar.TMMacroShiftCheck2 = 1
	end	
    TM_Set_ShiftButton2:Enable()
    SavedShiftString2:SetText("")
end

function TM_Set_ShiftButton2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton2_OnClick(self)
   self:Disable()
   SavedShiftString2:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck2, TM_Macro_ShiftCheck2, "shift-macrotext2", TauntMasterDBChar.SHIFTSPELL2, TauntMasterDBChar.SHIFTTARGET2) 
end



function TM_Macro_CtrlCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck2)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck2:SetChecked(nil)
		CtrlDropDown2_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck2 = 1
        TauntMasterDBChar.TMSpellCtrlCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck2:SetChecked(1)
		CtrlDropDown2_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck2 = 0
        TauntMasterDBChar.TMSpellCtrlCheck2 = 1
	end
	TM_Set_CtrlButton2:Enable()
    SavedCtrlString2:SetText("")
	
end

function TM_Spell_CtrlCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck2)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck2:SetChecked(nil)
		CtrlDropDown2_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck2 = 1
        TauntMasterDBChar.TMMacroCtrlCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck2:SetChecked(1)
		CtrlDropDown2_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck2 = 0
        TauntMasterDBChar.TMMacroCtrlCheck2 = 1
	end	
    TM_Set_CtrlButton2:Enable()
    SavedCtrlString2:SetText("")
end

function TM_Set_CtrlButton2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton2_OnClick(self)
   self:Disable()
   SavedCtrlString2:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck2, TM_Macro_CtrlCheck2, "ctrl-macrotext2", TauntMasterDBChar.CTRLSPELL2, TauntMasterDBChar.CTRLTARGET2) 
end


function TM_Macro_AltCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck2)         
	  end
    end)
             
end

function TM_Macro_AltCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck2:SetChecked(nil)
		AltDropDown2_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck2 = 1
        TauntMasterDBChar.TMSpellAltCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck2:SetChecked(1)
		AltDropDown2_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck2 = 0
        TauntMasterDBChar.TMSpellAltCheck2 = 1
	end
	TM_Set_AltButton2:Enable()
    SavedAltString2:SetText("")
	
end

function TM_Spell_AltCheck2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck2)        
	  end
    end)
             

end

function TM_Spell_AltCheck2_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck2:SetChecked(nil)
		AltDropDown2_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck2 = 1
        TauntMasterDBChar.TMMacroAltCheck2 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck2:SetChecked(1)
		AltDropDown2_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck2 = 0
        TauntMasterDBChar.TMMacroAltCheck2 = 1
	end	
    TM_Set_AltButton2:Enable()
    SavedAltString2:SetText("")
end

function TM_Set_AltButton2_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(2, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton2_OnClick(self)
   self:Disable()
   SavedAltString2:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck2, TM_Macro_AltCheck2, "alt-macrotext2", TauntMasterDBChar.ALTSPELL2, TauntMasterDBChar.ALTTARGET2) 
end



function TM_Macro_Check3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCheck3)         
	  end
    end)
             
end

function TM_Macro_Check3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_Check3:SetChecked(nil)
		DropDown3_Button:Hide()
        TauntMasterDBChar.TMMacroCheck3 = 1
        TauntMasterDBChar.TMSpellCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_Check3:SetChecked(1)
		DropDown3_Button:Show()
        TauntMasterDBChar.TMMacroCheck3 = 0
        TauntMasterDBChar.TMSpellCheck3 = 1
	end
	TM_Set_Button3:Enable()
    SavedString3:SetText("")
	
end

function TM_Spell_Check3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCheck3)        
	  end
    end)
             

end

function TM_Spell_Check3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_Check3:SetChecked(nil)
		DropDown3_Button:Show()
        TauntMasterDBChar.TMSpellCheck3 = 1
        TauntMasterDBChar.TMMacroCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_Check3:SetChecked(1)
		DropDown3_Button:Hide()
        TauntMasterDBChar.TMSpellCheck3 = 0
        TauntMasterDBChar.TMMacroCheck3 = 1
	end	
    TM_Set_Button3:Enable()
    SavedString3:SetText("")
end

function TM_Set_Button3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_Button3_OnClick(self)
   self:Disable()
   SavedString3:SetText("Saved")
   TM_Cycle(TM_Spell_Check3, TM_Macro_Check3, "macrotext3", TauntMasterDBChar.SPELL3, TauntMasterDBChar.TARGET3) 
end


function TM_Macro_ShiftCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck3)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck3:SetChecked(nil)
		ShiftDropDown3_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck3 = 1
        TauntMasterDBChar.TMSpellShiftCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck3:SetChecked(1)
		ShiftDropDown3_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck3 = 0
        TauntMasterDBChar.TMSpellShiftCheck3 = 1
	end
	TM_Set_ShiftButton3:Enable()
    SavedShiftString3:SetText("")
	
end

function TM_Spell_ShiftCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck3)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck3:SetChecked(nil)
		ShiftDropDown3_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck3 = 1
        TauntMasterDBChar.TMMacroShiftCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck3:SetChecked(1)
		ShiftDropDown3_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck3 = 0
        TauntMasterDBChar.TMMacroShiftCheck3 = 1
	end	
    TM_Set_ShiftButton3:Enable()
    SavedShiftString3:SetText("")
end

function TM_Set_ShiftButton3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton3_OnClick(self)
   self:Disable()
   SavedShiftString3:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck3, TM_Macro_ShiftCheck3, "shift-macrotext3", TauntMasterDBChar.SHIFTSPELL3, TauntMasterDBChar.SHIFTTARGET3) 
end



function TM_Macro_CtrlCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck3)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck3:SetChecked(nil)
		CtrlDropDown3_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck3 = 1
        TauntMasterDBChar.TMSpellCtrlCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck3:SetChecked(1)
		CtrlDropDown3_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck3 = 0
        TauntMasterDBChar.TMSpellCtrlCheck3 = 1
	end
	TM_Set_CtrlButton3:Enable()
    SavedCtrlString3:SetText("")
	
end

function TM_Spell_CtrlCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck3)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck3:SetChecked(nil)
		CtrlDropDown3_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck3 = 1
        TauntMasterDBChar.TMMacroCtrlCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck3:SetChecked(1)
		CtrlDropDown3_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck3 = 0
        TauntMasterDBChar.TMMacroCtrlCheck3 = 1
	end	
    TM_Set_CtrlButton3:Enable()
    SavedCtrlString3:SetText("")
end

function TM_Set_CtrlButton3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton3_OnClick(self)
   self:Disable()
   SavedCtrlString3:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck3, TM_Macro_CtrlCheck3, "ctrl-macrotext3", TauntMasterDBChar.CTRLSPELL3, TauntMasterDBChar.CTRLTARGET3) 
end


function TM_Macro_AltCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck3)         
	  end
    end)
             
end

function TM_Macro_AltCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck3:SetChecked(nil)
		AltDropDown3_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck3 = 1
        TauntMasterDBChar.TMSpellAltCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck3:SetChecked(1)
		AltDropDown3_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck3 = 0
        TauntMasterDBChar.TMSpellAltCheck3 = 1
	end
	TM_Set_AltButton3:Enable()
    SavedAltString3:SetText("")
	
end

function TM_Spell_AltCheck3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck3)        
	  end
    end)
             

end

function TM_Spell_AltCheck3_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck3:SetChecked(nil)
		AltDropDown3_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck3 = 1
        TauntMasterDBChar.TMMacroAltCheck3 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck3:SetChecked(1)
		AltDropDown3_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck3 = 0
        TauntMasterDBChar.TMMacroAltCheck3 = 1
	end	
    TM_Set_AltButton3:Enable()
    SavedAltString3:SetText("")
end

function TM_Set_AltButton3_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(3, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton3_OnClick(self)
   self:Disable()
   SavedAltString3:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck3, TM_Macro_AltCheck3, "alt-macrotext3", TauntMasterDBChar.ALTSPELL3, TauntMasterDBChar.ALTTARGET3) 
end



function TM_Macro_Check4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCheck4)         
	  end
    end)
             
end

function TM_Macro_Check4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_Check4:SetChecked(nil)
		DropDown4_Button:Hide()
        TauntMasterDBChar.TMMacroCheck4 = 1
        TauntMasterDBChar.TMSpellCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_Check4:SetChecked(1)
		DropDown4_Button:Show()
        TauntMasterDBChar.TMMacroCheck4 = 0
        TauntMasterDBChar.TMSpellCheck4 = 1
	end
	TM_Set_Button4:Enable()
    SavedString4:SetText("")
	
end

function TM_Spell_Check4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCheck4)        
	  end
    end)
             

end

function TM_Spell_Check4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_Check4:SetChecked(nil)
		DropDown4_Button:Show()
        TauntMasterDBChar.TMSpellCheck4 = 1
        TauntMasterDBChar.TMMacroCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_Check4:SetChecked(1)
		DropDown4_Button:Hide()
        TauntMasterDBChar.TMSpellCheck4 = 0
        TauntMasterDBChar.TMMacroCheck4 = 1
	end	
    TM_Set_Button4:Enable()
    SavedString4:SetText("")
end

function TM_Set_Button4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_Button4_OnClick(self)
   self:Disable()
   SavedString4:SetText("Saved")
   TM_Cycle(TM_Spell_Check4, TM_Macro_Check4, "macrotext4", TauntMasterDBChar.SPELL4, TauntMasterDBChar.TARGET4) 
end


function TM_Macro_ShiftCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck4)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck4:SetChecked(nil)
		ShiftDropDown4_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck4 = 1
        TauntMasterDBChar.TMSpellShiftCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck4:SetChecked(1)
		ShiftDropDown4_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck4 = 0
        TauntMasterDBChar.TMSpellShiftCheck4 = 1
	end
	TM_Set_ShiftButton4:Enable()
    SavedShiftString4:SetText("")
	
end

function TM_Spell_ShiftCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck4)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck4:SetChecked(nil)
		ShiftDropDown4_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck4 = 1
        TauntMasterDBChar.TMMacroShiftCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck4:SetChecked(1)
		ShiftDropDown4_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck4 = 0
        TauntMasterDBChar.TMMacroShiftCheck4 = 1
	end	
    TM_Set_ShiftButton4:Enable()
    SavedShiftString4:SetText("")
end

function TM_Set_ShiftButton4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton4_OnClick(self)
   self:Disable()
   SavedShiftString4:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck4, TM_Macro_ShiftCheck4, "shift-macrotext4", TauntMasterDBChar.SHIFTSPELL4, TauntMasterDBChar.SHIFTTARGET4) 
end



function TM_Macro_CtrlCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck4)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck4:SetChecked(nil)
		CtrlDropDown4_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck4 = 1
        TauntMasterDBChar.TMSpellCtrlCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck4:SetChecked(1)
		CtrlDropDown4_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck4 = 0
        TauntMasterDBChar.TMSpellCtrlCheck4 = 1
	end
	TM_Set_CtrlButton4:Enable()
    SavedCtrlString4:SetText("")
	
end

function TM_Spell_CtrlCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck4)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck4:SetChecked(nil)
		CtrlDropDown4_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck4 = 1
        TauntMasterDBChar.TMMacroCtrlCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck4:SetChecked(1)
		CtrlDropDown4_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck4 = 0
        TauntMasterDBChar.TMMacroCtrlCheck4 = 1
	end	
    TM_Set_CtrlButton4:Enable()
    SavedCtrlString4:SetText("")
end

function TM_Set_CtrlButton4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton4_OnClick(self)
   self:Disable()
   SavedCtrlString4:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck4, TM_Macro_CtrlCheck4, "ctrl-macrotext4", TauntMasterDBChar.CTRLSPELL4, TauntMasterDBChar.CTRLTARGET4) 
end


function TM_Macro_AltCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck4)         
	  end
    end)
             
end

function TM_Macro_AltCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck4:SetChecked(nil)
		AltDropDown4_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck4 = 1
        TauntMasterDBChar.TMSpellAltCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck4:SetChecked(1)
		AltDropDown4_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck4 = 0
        TauntMasterDBChar.TMSpellAltCheck4 = 1
	end
	TM_Set_AltButton4:Enable()
    SavedAltString4:SetText("")
	
end

function TM_Spell_AltCheck4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck4)        
	  end
    end)
             

end

function TM_Spell_AltCheck4_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck4:SetChecked(nil)
		AltDropDown4_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck4 = 1
        TauntMasterDBChar.TMMacroAltCheck4 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck4:SetChecked(1)
		AltDropDown4_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck4 = 0
        TauntMasterDBChar.TMMacroAltCheck4 = 1
	end	
    TM_Set_AltButton4:Enable()
    SavedAltString4:SetText("")
end

function TM_Set_AltButton4_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(4, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton4_OnClick(self)
   self:Disable()
   SavedAltString4:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck4, TM_Macro_AltCheck4, "alt-macrotext4", TauntMasterDBChar.ALTSPELL4, TauntMasterDBChar.ALTTARGET4) 
end


function TM_Macro_Check5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCheck5)         
	  end
    end)
             
end

function TM_Macro_Check5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_Check5:SetChecked(nil)
		DropDown5_Button:Hide()
        TauntMasterDBChar.TMMacroCheck5 = 1
        TauntMasterDBChar.TMSpellCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_Check5:SetChecked(1)
		DropDown5_Button:Show()
        TauntMasterDBChar.TMMacroCheck5 = 0
        TauntMasterDBChar.TMSpellCheck5 = 1
	end
	TM_Set_Button5:Enable()
    SavedString5:SetText("")
	
end

function TM_Spell_Check5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCheck5)        
	  end
    end)
             

end

function TM_Spell_Check5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_Check5:SetChecked(nil)
		DropDown5_Button:Show()
        TauntMasterDBChar.TMSpellCheck5 = 1
        TauntMasterDBChar.TMMacroCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_Check5:SetChecked(1)
		DropDown5_Button:Hide()
        TauntMasterDBChar.TMSpellCheck5 = 0
        TauntMasterDBChar.TMMacroCheck5 = 1
	end	
    TM_Set_Button5:Enable()
    SavedString5:SetText("")
end

function TM_Set_Button5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_Button5_OnClick(self)
   self:Disable()
   SavedString5:SetText("Saved")
   TM_Cycle(TM_Spell_Check5, TM_Macro_Check5, "macrotext5", TauntMasterDBChar.SPELL5, TauntMasterDBChar.TARGET5) 
end


function TM_Macro_ShiftCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroShiftCheck5)         
	  end
    end)
             
end

function TM_Macro_ShiftCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_ShiftCheck5:SetChecked(nil)
		ShiftDropDown5_Button:Hide()
        TauntMasterDBChar.TMMacroShiftCheck5 = 1
        TauntMasterDBChar.TMSpellShiftCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_ShiftCheck5:SetChecked(1)
		ShiftDropDown5_Button:Show()
        TauntMasterDBChar.TMMacroShiftCheck5 = 0
        TauntMasterDBChar.TMSpellShiftCheck5 = 1
	end
	TM_Set_ShiftButton5:Enable()
    SavedShiftString5:SetText("")
	
end

function TM_Spell_ShiftCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellShiftCheck5)        
	  end
    end)
             

end

function TM_Spell_ShiftCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_ShiftCheck5:SetChecked(nil)
		ShiftDropDown5_Button:Show()
        TauntMasterDBChar.TMSpellShiftCheck5 = 1
        TauntMasterDBChar.TMMacroShiftCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_ShiftCheck5:SetChecked(1)
		ShiftDropDown5_Button:Hide()
        TauntMasterDBChar.TMSpellShiftCheck5 = 0
        TauntMasterDBChar.TMMacroShiftCheck5 = 1
	end	
    TM_Set_ShiftButton5:Enable()
    SavedShiftString5:SetText("")
end

function TM_Set_ShiftButton5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_ShiftButton5_OnClick(self)
   self:Disable()
   SavedShiftString5:SetText("Saved")
   TM_Cycle(TM_Spell_ShiftCheck5, TM_Macro_ShiftCheck5, "shift-macrotext5", TauntMasterDBChar.SHIFTSPELL5, TauntMasterDBChar.SHIFTTARGET5) 
end



function TM_Macro_CtrlCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroCtrlCheck5)         
	  end
    end)
             
end

function TM_Macro_CtrlCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_CtrlCheck5:SetChecked(nil)
		CtrlDropDown5_Button:Hide()
        TauntMasterDBChar.TMMacroCtrlCheck5 = 1
        TauntMasterDBChar.TMSpellCtrlCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_CtrlCheck5:SetChecked(1)
		CtrlDropDown5_Button:Show()
        TauntMasterDBChar.TMMacroCtrlCheck5 = 0
        TauntMasterDBChar.TMSpellCtrlCheck5 = 1
	end
	TM_Set_CtrlButton5:Enable()
    SavedCtrlString5:SetText("")
	
end

function TM_Spell_CtrlCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellCtrlCheck5)        
	  end
    end)
             

end

function TM_Spell_CtrlCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_CtrlCheck5:SetChecked(nil)
		CtrlDropDown5_Button:Show()
        TauntMasterDBChar.TMSpellCtrlCheck5 = 1
        TauntMasterDBChar.TMMacroCtrlCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_CtrlCheck5:SetChecked(1)
		CtrlDropDown5_Button:Hide()
        TauntMasterDBChar.TMSpellCtrlCheck5 = 0
        TauntMasterDBChar.TMMacroCtrlCheck5 = 1
	end	
    TM_Set_CtrlButton5:Enable()
    SavedCtrlString5:SetText("")
end

function TM_Set_CtrlButton5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_CtrlButton5_OnClick(self)
   self:Disable()
   SavedCtrlString5:SetText("Saved")
   TM_Cycle(TM_Spell_CtrlCheck5, TM_Macro_CtrlCheck5, "ctrl-macrotext5", TauntMasterDBChar.CTRLSPELL5, TauntMasterDBChar.CTRLTARGET5) 
end


function TM_Macro_AltCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMMacroAltCheck5)         
	  end
    end)
             
end

function TM_Macro_AltCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Spell_AltCheck5:SetChecked(nil)
		AltDropDown5_Button:Hide()
        TauntMasterDBChar.TMMacroAltCheck5 = 1
        TauntMasterDBChar.TMSpellAltCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Spell_AltCheck5:SetChecked(1)
		AltDropDown5_Button:Show()
        TauntMasterDBChar.TMMacroAltCheck5 = 0
        TauntMasterDBChar.TMSpellAltCheck5 = 1
	end
	TM_Set_AltButton5:Enable()
    SavedAltString5:SetText("")
	
end

function TM_Spell_AltCheck5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
      self:SetChecked(TauntMasterDBChar.TMSpellAltCheck5)        
	  end
    end)
             

end

function TM_Spell_AltCheck5_OnClick(self)
	if self:GetChecked() == 1 then
		TM_Macro_AltCheck5:SetChecked(nil)
		AltDropDown5_Button:Show()
        TauntMasterDBChar.TMSpellAltCheck5 = 1
        TauntMasterDBChar.TMMacroAltCheck5 = 0
	end
	if self:GetChecked() ~= 1 then
		TM_Macro_AltCheck5:SetChecked(1)
		AltDropDown5_Button:Hide()
        TauntMasterDBChar.TMSpellAltCheck5 = 0
        TauntMasterDBChar.TMMacroAltCheck5 = 1
	end	
    TM_Set_AltButton5:Enable()
    SavedAltString5:SetText("")
end

function TM_Set_AltButton5_OnLoad(self)
self:RegisterEvent('ADDON_LOADED')
    self:SetScript('OnEvent', function(self, event, ...)
      if event == 'ADDON_LOADED' and ... == 'TauntMaster' then
        local class = select(5, UnitClass('player'))
        if class == "PALADIN" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(paladindefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "WARRIOR" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(warriordefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DEATHKNIGHT" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(deathknightdefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        elseif class == "DRUID" then
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(druiddefaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
        else
                TauntMasterDBChar = TauntMasterDBChar or {}
                for option, value in pairs(defaults) do
                    if not TauntMasterDBChar[option] then TauntMasterDBChar[option] = value end
                end
       end
         
	  end
    end)
end

function TM_Set_AltButton5_OnClick(self)
   self:Disable()
   SavedAltString5:SetText("Saved")
   TM_Cycle(TM_Spell_AltCheck5, TM_Macro_AltCheck5, "alt-macrotext5", TauntMasterDBChar.ALTSPELL5, TauntMasterDBChar.ALTTARGET5) 
end
