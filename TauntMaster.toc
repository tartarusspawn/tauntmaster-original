## Interface: 40000
## Title: TauntMaster
## Author: Prodigy
## SavedVariablesPerCharacter: TauntMasterDBChar
## SavedVariables: TauntMasterDB
## Version: 4.1.0
## Notes: A frame that detects aggro and allows the user to easily taunt off party/raid members.

TauntMaster.lua
TauntMaster.xml
